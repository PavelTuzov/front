import React, { FC } from 'react';
import { Button, H3, Input, PhoneInput, Checkbox, CheckBoxSizeTypes, RadioSizeTypes, Radio, InputGroup, InputSizeTypes, ButtonSizeTypes, Text } from 'front.ui.lib';

export const TmpForm: FC = () => {
  return (
    <div>
      <form>
        <InputGroup>
          <InputGroup>
            <Input type={"text"} tabIndex={0} label={'Имя'} styleSize={InputSizeTypes.LARGE} placeholder={"Василий"}/>
          </InputGroup>
          <InputGroup>
            <Input type={"text"} tabIndex={0} label={'Фамилия'} styleSize={InputSizeTypes.LARGE} placeholder={"Пупкин"}/>
          </InputGroup>
          <InputGroup>
            <Input type={"text"} tabIndex={0} label={'Отчество'} styleSize={InputSizeTypes.LARGE} placeholder={"Батькович"}/>
          </InputGroup>
        </InputGroup>
        <InputGroup>
          <InputGroup>
            <Input type={"text"} tabIndex={0} label={'Имя'} styleSize={InputSizeTypes.DEFAULT} placeholder={"Василий"}/>
          </InputGroup>
          <InputGroup>
            <Input type={"text"} tabIndex={0} label={'Фамилия'} styleSize={InputSizeTypes.DEFAULT} placeholder={"Пупкин"}/>
          </InputGroup>
          <InputGroup>
            <Input type={"text"} tabIndex={0} label={'Отчество'} styleSize={InputSizeTypes.DEFAULT} placeholder={"Батькович"}/>
          </InputGroup>
        </InputGroup>
        <InputGroup>
          <InputGroup>
            <Input type={"text"} tabIndex={0} label={'Имя'} styleSize={InputSizeTypes.SMALL} placeholder={"Василий"}/>
          </InputGroup>
          <InputGroup>
            <Input type={"text"} tabIndex={0} label={'Фамилия'} styleSize={InputSizeTypes.SMALL} placeholder={"Пупкин"}/>
          </InputGroup>
          <InputGroup>
            <Input type={"text"} tabIndex={0} label={'Отчество'} styleSize={InputSizeTypes.SMALL} placeholder={"Батькович"}/>
          </InputGroup>
        </InputGroup>
        <InputGroup>
          <Input type={"text"} tabIndex={0} label={'Текстовое поле'} styleSize={InputSizeTypes.LARGE} placeholder={"Рандомная строка"} width={"100%"} disabled/>
        </InputGroup>

        <InputGroup>
          <InputGroup>
            <Checkbox tabIndex={0} styleSize={CheckBoxSizeTypes.SMALL}>Включить опцию</Checkbox>
          </InputGroup>

          <InputGroup>
            <Checkbox tabIndex={0} disabled={true} styleSize={CheckBoxSizeTypes.SMALL}>Ничего не поделать</Checkbox>
          </InputGroup>
        </InputGroup>

        <InputGroup>
          <InputGroup>
            <Checkbox tabIndex={0} checked={true}>Выключить опцию</Checkbox>
          </InputGroup>

          <InputGroup>
            <Checkbox tabIndex={0} disabled={true} checked={true}>Ничего не поделать</Checkbox>
          </InputGroup>
        </InputGroup>

        <InputGroup>
          <InputGroup>
            <Checkbox tabIndex={0} styleSize={CheckBoxSizeTypes.LARGE}>Включить опцию</Checkbox>
          </InputGroup>

          <InputGroup>
            <Checkbox tabIndex={0} checked={true} styleSize={CheckBoxSizeTypes.LARGE} disabled={true}>Выключить опцию</Checkbox>
          </InputGroup>
        </InputGroup>

        <InputGroup>
          <InputGroup>
            <Radio tabIndex={0} name={"block_1"} styleSize={RadioSizeTypes.SMALL}>Включить опцию</Radio>
          </InputGroup>

          <InputGroup>
            <Radio tabIndex={0} name={"block_2"} disabled={true} styleSize={RadioSizeTypes.SMALL}>Ничего не поделать</Radio>
          </InputGroup>
        </InputGroup>

        <InputGroup>
          <InputGroup>
            <Radio tabIndex={0} name={"block_1"} checked={true}>Выключить опцию</Radio>
          </InputGroup>

          <InputGroup>
            <Radio tabIndex={0} name={"block_2"} disabled={true} checked={true}>Ничего не поделать</Radio>
          </InputGroup>
        </InputGroup>

        <InputGroup>
          <InputGroup>
            <Radio tabIndex={0} name={"block_1"} styleSize={RadioSizeTypes.LARGE}>Включить опцию</Radio>
          </InputGroup>

          <InputGroup>
            <Radio tabIndex={0} name={"block_2"} checked={true} styleSize={RadioSizeTypes.LARGE} disabled={true}>Выключить опцию</Radio>
          </InputGroup>
        </InputGroup>

        <InputGroup>
          <InputGroup><Button>asd</Button></InputGroup>
        </InputGroup>
      </form>
    </div>
  );
};