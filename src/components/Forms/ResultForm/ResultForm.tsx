import {FC} from 'react';
import { ResultFormProps } from "~/components/Forms/ResultForm/interfaces";
import { SuccessIcon } from "~/components/icons";
import { H1, H3, Text } from "front.ui.lib";
import { StyledResultForm } from "~/components/Forms/ResultForm/styles";

const ResultForm:FC<ResultFormProps> = ({ title, subTitle, result} : ResultFormProps) => {
  return (
    <StyledResultForm>
      <SuccessIcon />
      <H1>{title}</H1>
      {subTitle && (<H3>{subTitle}</H3>)}

    </StyledResultForm>
  );
};

export default ResultForm;