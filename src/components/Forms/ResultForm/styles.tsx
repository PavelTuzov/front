import styled from "@emotion/styled";
import { defaultTheme } from "front.ui.lib";


export const StyledResultForm = styled.div`
  text-align: center;
  & svg {
    margin: 0 auto 2rem auto;
    width: 4rem;
    height: 4rem;
    display: block;
    fill: ${defaultTheme.colors.success};
  }
  & h1 {
    font-size: 2rem;
    line-height: 2.2rem;
    font-weight: 400;
  }
`