export interface ResultFormProps {
  title: string
  subTitle?: string
  result: FormResult
}

export enum FormResult {
  SUCCESS,
  ERROR,
}