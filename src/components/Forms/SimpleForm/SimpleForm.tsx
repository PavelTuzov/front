import React from 'react';
import { Button, H3, Input, PhoneInput, Checkbox, CheckBoxSizeTypes, RadioSizeTypes, Radio, InputGroup, InputSizeTypes, ButtonSizeTypes } from 'front.ui.lib';

const SimpleForm = () => {
  return (
    <div>
      <form>
        <H3>Остались вопросы?</H3>
        <InputGroup>
          <InputGroup>
            <Input type={"text"} tabIndex={0} label={'Имя'} styleSize={InputSizeTypes.LARGE} placeholder={"Василий"}/>
          </InputGroup>
        </InputGroup>

        <InputGroup>
          <InputGroup>
            <Input type="phone" tabIndex={0} label={'Телефон'} styleSize={InputSizeTypes.LARGE} placeholder={"Телефон"}/>
          </InputGroup>
        </InputGroup>

        <InputGroup>
          <InputGroup><Button styleSize={ButtonSizeTypes.LARGE}>Задать вопрос</Button></InputGroup>
        </InputGroup>
      </form>
    </div>
  );
};

export default SimpleForm;