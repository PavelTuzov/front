export interface Errors {
  phone: string
  name: string
  email: string
  agreement: string
}

export interface ConsultFormProps {
  onFinish: () => void
  selectDepartment: (number) => void;
  selectedDepartment: number;
}