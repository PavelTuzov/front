import styled from "@emotion/styled";
import { defaultTheme } from 'front.ui.lib';

export const StyledSubtitle = styled.p`
  padding: 2rem;
  background-color: ${defaultTheme.colors.info};
  margin: 0 0 2rem 0;
  border-radius: 2px;
`;
