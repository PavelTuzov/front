import React, {FC, useEffect, useState} from 'react';
import { Button, UsedLocales, Input, Note, Checkbox, DropDown, DropDownSizeTypes, DropDownItemProps, InputGroup, InputSizeTypes, ButtonSizeTypes, Text, Container, Row } from 'front.ui.lib';
import { Trans, useTranslation } from "next-i18next";
import { StyledSubtitle } from "./styles";
import {getDepartments, makeFormOrder} from "~/api/api";
import {ConsultFormResponse } from "~/components/Page/types";
import { ConsultFormProps, Errors } from "./interfaces";

export const ConsultForm: FC<ConsultFormProps> = ({ onFinish, selectDepartment, selectedDepartment }: ConsultFormProps) => {
  const { t, i18n } = useTranslation('common');
  const lang = i18n.language;

  const FORM_NAME = 'consultForm';

  const [agreement, setAgreement] = useState<boolean>(true);
  const [name, setName] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const [phone, setPhone] = useState<string>('');
  const [departments, setDepartments] = useState<DropDownItemProps[]>([]);
  const [department, setDepartment] = useState<DropDownItemProps>();

  const [isSuccess, setIsSuccess] = useState<boolean>(false);
  const [isDepartmentLoading, setIsDepartmentLoading] = useState<boolean>(true);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [response, setResponse] = useState<ConsultFormResponse>(null)
  const [isDisabled, setIsDisabled] = useState<boolean>(true);
  const [errors, setErrors] = useState<Errors>({
    name: null,
    phone: null,
    email: null,
    agreement: null
  });

  const handleChangeAgree = (index, e) => {
    const value = e.target.value;
    const isChecked = e.target.checked;
  }

  const handleChangeDepartment = (item: DropDownItemProps) => {
    setDepartment(item);
    selectDepartment(item.key)
    return false;
  }

  const handleSubmit = async (e) => {
    e.preventDefault();

    setIsLoading(true);
    setResponse(null);

    const data = new FormData();
    data.append('name', name);
    data.append('phone', phone);
    data.append('email', email);
    data.append('type', FORM_NAME);
    data.append('department', department.key);

    try {
      const res = await makeFormOrder(data);
      setResponse(res);
    } catch (e) {
      console.log(e ?? null)
    }
  }

  useEffect(() => {
    setIsLoading(false);
    setIsSuccess(true);
  }, [response]);

  useEffect(() => {
    if (isSuccess) {
      onFinish();
    }
  }, [isSuccess])

  useEffect(() => {
    setIsDepartmentLoading(true);
    setDepartments([]);
    setIsSuccess(false);
    setErrors({...errors, name: undefined}); // Autofocus provided

    const fetchDepsData = async () => {
      const resultRaw = await getDepartments();
      const result = resultRaw.map(item => {
        const dep: DropDownItemProps = {
          key: item.id.toString(),
          value: lang === UsedLocales.ru ? item.title : item.title_en,
        }

        return dep;
      });
      setDepartments(result);
      setDepartment(result.find(item => item.key === selectedDepartment.toString()));
    }

    fetchDepsData()
      .catch(console.error)
  }, []);

  useEffect(() => {
    if (department) {
      setIsDepartmentLoading(false);
    }
  }, [department])

  useEffect(() => {
    if (!agreement) {
      setErrors({...errors, agreement: t('errors.agreement')});
    } else {
      setErrors({...errors, agreement: undefined});
    }
  }, [agreement]);

  useEffect(() => {
    setIsDisabled(!(errors.phone === undefined && errors.name === undefined && errors.agreement === undefined) || isDepartmentLoading);
  }, [errors, isDepartmentLoading])

  return (
    <div>
      <form onSubmit={(e) => handleSubmit(e)}>
        <StyledSubtitle>
          {t('consultForm.subTitle')}
        </StyledSubtitle>
        <Container gap={2}>
          <Row col={5}>

            <InputGroup>
              <Input
                type={"text"}
                tabIndex={0}
                label={t('consultForm.inputs.name.label')}
                styleSize={InputSizeTypes.LARGE}
                placeholder={t('consultForm.inputs.name.placeholder')}
                value={name}
                error={errors.name}
                onChange={(e): void => {
                  setName(e.target.value);
                }}
                onBlur={(e): void => {
                  if (e.target.value.length === 0) {
                    setErrors({...errors, name: t('errors.name')});
                  }
                }}
                onFocus={(e): void => {
                  setErrors({...errors, name: undefined});
                }}
              />
            </InputGroup>
            <InputGroup>
              <Input
                type={"phone"}
                tabIndex={0}
                label={t('consultForm.inputs.phone.label')}
                styleSize={InputSizeTypes.LARGE}
                placeholder={t('consultForm.inputs.phone.placeholder')}
                value={phone}
                error={errors.phone}
                onChangeCustomInput={(event): void => {
                  setPhone(event.value);
                  setErrors({...errors, phone: event.errorText ? t('errors.phone') : undefined});
                }}
              />
            </InputGroup>
          </Row>
          <Row col={5}>
            <InputGroup>
              <DropDown
                isLoading={isDepartmentLoading}
                label={t('consultForm.inputs.department.label')}
                options={!isDepartmentLoading && departments}
                defaultOption={!isDepartmentLoading && department}
                handleChange={handleChangeDepartment}
                tabIndex={0}
                styleSize={DropDownSizeTypes.LARGE}
              />
            </InputGroup>
            <InputGroup>
              <Input
                type={"text"}
                tabIndex={0}
                label={t('consultForm.inputs.email.label')}
                styleSize={InputSizeTypes.LARGE}
                placeholder={t('consultForm.inputs.email.placeholder')}
                value={email}
                error={errors.email}
                onChange={(e): void => {
                  setEmail(e.target.value);
                }}
                onBlur={(e): void => {
                  if (e.target.value.length === 0) {
                    setErrors({...errors, email: t('errors.email')});
                  }
                }}
                onFocus={(e): void => {
                  setErrors({...errors, email: undefined});
                }}
              />
            </InputGroup>
          </Row>
        </Container>
        <InputGroup>
          <Checkbox name={"agreement"} error={errors.agreement} onChange={() => setAgreement(!agreement)} checked={agreement}>
            <Note>
              <Trans i18nKey="consultForm.agreement">
                Я даю свое согласие на обработку персональных данных, в соответствии с условиями, изложенными в <a href={"/agreement"} target={"_blank"}>Согласии на обработку персональных данных</a> и <a href={"/privacy"} target={"_blank"}>Политике обработки персональных данных</a>
              </Trans>
            </Note>
          </Checkbox>
        </InputGroup>
        <InputGroup>
          <InputGroup>
            <Button isLoading={isLoading} disabled={isDisabled} styleSize={ButtonSizeTypes.LARGE}>{t('consultForm.submit')}</Button>
          </InputGroup>
        </InputGroup>
      </form>
    </div>
  );
};