import React, {FC, useEffect, useState} from 'react';
import { Button, H3, Input, Note, Checkbox, CheckBoxSizeTypes, RadioSizeTypes, Radio, InputGroup, InputSizeTypes, ButtonSizeTypes, Text, Container, Row } from 'front.ui.lib';
import { Trans, useTranslation } from "next-i18next";
import { StyledSubtitle, StyledTimeList, StyledTimeText} from "./styles";
import { ClockIcon } from "~/components/icons";
import { makeFormOrder } from "~/api/api";
import { RecallFormResponse } from "~/components/Page/types";
import {Errors, RecallFormProps} from "~/components/Forms/RecallForm/interfaces";

export const RecallForm: FC<RecallFormProps> = ({ onFinish }: RecallFormProps) => {
  const FORM_NAME = 'recallForm';
  const TIME_PERIODS = [
    '10:00 — 13:00',
    '13:00 — 16:00',
    '16:00 – 19:00'
  ]

  const [agreement, setAgreement] = useState<boolean>(true);
  const [name, setName] = useState<string>('');
  const [phone, setPhone] = useState<string>('');
  const [selectedTime] = useState<number>(1);
  const [time, setTime] = useState<string[]>([TIME_PERIODS[selectedTime]]);

  const [isSuccess, setIsSuccess] = useState<boolean>(false);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [response, setResponse] = useState<RecallFormResponse|null>(null)
  const [isDisabled, setIsDisabled] = useState<boolean>(true);
  const [errors, setErrors] = useState<Errors>({
    name: null,
    phone: null,
    agreement: null
  });

  const { t } = useTranslation('common');

  const handleChangeAgree = (index, e) => {
    const timeData = time;
    const value = e.target.value;
    const isChecked = e.target.checked;
    if (isChecked) {
      timeData[index] = value;
    } else {
      delete timeData[index];
    }

    setTime(timeData);
  }

  const handleSubmit = async (e) => {
    e.preventDefault();

    setIsLoading(true);
    setResponse(null);

    const data = new FormData();
    data.append('name', name);
    data.append('phone', phone);
    data.append('time', time.join(', '));
    data.append('type', FORM_NAME);

    try {
      const res = await makeFormOrder(data);
      setResponse(res);
    } catch (e) {
      console.log(e ?? null)
    }
  }

  useEffect(() => {
    setIsLoading(false);
    setIsSuccess(true);
  }, [response]);

  useEffect(() => {
    if (isSuccess) {
      onFinish();
    }
  }, [isSuccess])

  useEffect(() => {
    setIsSuccess(false);
    setErrors({...errors, name: undefined}); // Autofocus provided
  }, [])

  useEffect(() => {
    if (!agreement) {
      setErrors({...errors, agreement: t('errors.agreement')});
    } else {
      setErrors({...errors, agreement: undefined});
    }
  }, [agreement]);

  useEffect(() => {
    setIsDisabled(!(errors.phone === undefined && errors.name === undefined  && errors.agreement === undefined));
  }, [errors])

  return (
    <div>
      <form onSubmit={(e) => handleSubmit(e)}>
        <StyledSubtitle>
          {t('recallForm.subTitle')}
        </StyledSubtitle>
        <Container gap={2}>
          <Row col={6}>
            <InputGroup>
              <Input
                type={"text"}
                tabIndex={0}
                label={t('recallForm.inputs.name.label')}
                styleSize={InputSizeTypes.LARGE}
                placeholder={t('recallForm.inputs.name.placeholder')}
                value={name}
                error={errors.name}
                onChange={(e): void => {
                  setName(e.target.value);
                }}
                onBlur={(e): void => {
                  if (e.target.value.length === 0) {
                    setErrors({...errors, name: t('errors.name')});
                  }
                }}
                onFocus={(e): void => {
                  setErrors({...errors, name: undefined});
                }}
              />
            </InputGroup>
            <InputGroup>
              <Input
                type={"phone"}
                tabIndex={0}
                label={t('recallForm.inputs.phone.label')}
                styleSize={InputSizeTypes.LARGE}
                placeholder={t('recallForm.inputs.phone.placeholder')}
                value={phone}
                error={errors.phone}
                onChangeCustomInput={(event): void => {
                  setPhone(event.value);
                  setErrors({...errors, phone: event.errorText ? t('errors.phone') : undefined});
                }}
              />
            </InputGroup>
          </Row>
          <Row col={4}>
            <StyledTimeText>
              <ClockIcon/> {t('recallForm.when')} ({t('recallForm.time')})
            </StyledTimeText>
            <StyledTimeList>
              {TIME_PERIODS.map((period, index) => (
                <li key={index}>
                  <InputGroup>
                    <Checkbox
                      name={"time"}
                      value={period}
                      onChange={(e) => handleChangeAgree(index, e)}
                      defaultChecked={index === 1}
                    >{period}</Checkbox>
                  </InputGroup>
                </li>
              ))}
            </StyledTimeList>
          </Row>
        </Container>
        <InputGroup>
          <Checkbox name={"agreement"} error={errors.agreement} onChange={() => setAgreement(!agreement)} checked={agreement}>
            <Note>
              <Trans i18nKey="recallForm.agreement">
                Я даю свое согласие на обработку персональных данных, в соответствии с условиями, изложенными в <a href={"/agreement"} target={"_blank"}>Согласии на обработку персональных данных</a> и <a href={"/privacy"} target={"_blank"}>Политике обработки персональных данных</a>
              </Trans>
            </Note>
          </Checkbox>
        </InputGroup>
        <InputGroup>
          <InputGroup>
            <Button isLoading={isLoading} disabled={isDisabled} styleSize={ButtonSizeTypes.LARGE}>{t('recallForm.submit')}</Button>
          </InputGroup>
        </InputGroup>
      </form>
    </div>
  );
};