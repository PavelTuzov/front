import styled from "@emotion/styled";
import { defaultTheme } from 'front.ui.lib';

export const StyledSubtitle = styled.p`
  padding: 2rem;
  background-color: ${defaultTheme.colors.info};
  margin: 0 0 2rem 0;
  border-radius: 2px;
`;

export const StyledTimeList = styled.ul`
  padding: 0;
  list-style: none;
  & > li {
    
  }
`;

export const StyledTimeText = styled.p`
  padding: 0;
  display: flex;
  align-content: center;
  align-items: center;
  margin: 0 0 1rem 0;
  & svg {
    margin: 0 0.5rem 0 0;
    max-width: 24px;
    min-width: 24px;
    max-height: 24px;
    fill: ${defaultTheme.colors.darkGrey};
  }
`;