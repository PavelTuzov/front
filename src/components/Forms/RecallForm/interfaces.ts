export interface Errors {
  phone: string
  name: string
  agreement: string
}

export interface RecallFormProps {
  onFinish: () => void
}