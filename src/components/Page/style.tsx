import styled from '@emotion/styled';

/** константы */
import { desktop940 } from '../Grid/constants';

export const ContentContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  height: 100%;
  max-width: 100%;
  overflow-x: hidden;
`;

export const InnerContentContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  position: relative;
  flex: 1 0 auto;
  width: 100%;
  padding-bottom: 64px;
  max-width: 1440px;

  .content {
    padding-top: 100px;
  }
`;
