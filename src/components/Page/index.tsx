import * as React from 'react';
import { ContentContainer, InnerContentContainer } from './style';
import { ContentProps } from './types';

const Content: React.FC<ContentProps> = (props: ContentProps) => {
  const { children } = props;
  return (
    <ContentContainer>
      <InnerContentContainer role="main">
        {children}
      </InnerContentContainer>
    </ContentContainer>
  );
};

export default Content;
