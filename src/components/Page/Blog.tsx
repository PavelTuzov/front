/** Библиотеки */
import { useCallback, useContext, useEffect, useState } from 'react';
import { useMediaQuery } from 'react-responsive';

import * as React from 'react';

/** Компоненты */
//import { SidePage } from 'front.ui.lib';
import Content from './index';
//import GlobalPreloader from '../Preloader';
import AuthWizard from '../Wizards/AuthWizard';
import NotFound from './NotFound';

/** Контексты */
import { AuthContext } from '../Providers/AuthProvider';

/** Константы */
import { desktop940 } from '../Grid/constants';
import {
  ERROR_MESSAGE,
} from '../Wizards/AuthWizard/constants';

/** интерфейсы */
import {PageBlogProps, PageProps} from './types';
import {useTranslation} from "next-i18next";

/** Страница ленты */
const Blog: React.FC<PageBlogProps> = ({
                                     page,
                                     menu,
                                     preview,
  posts
                                   }: PageBlogProps) => {

  const { i18n } = useTranslation('common');
  const lang = i18n.language;
  const blocks = page.blocks;


  const {
    auth,
    toggleVisible: toggleAuthSidePage,
    isVisible: isAuthOpen,
    onCloseClickSidePageAuth,
  } = useContext(AuthContext);

  return (
    <>
      <Content>
        <ul>
          {posts.map((item, index) => (<li key={item.id}>{item.title}</li>))}
        </ul>
      </Content>
    </>
  );
};

export default React.memo(Blog);
