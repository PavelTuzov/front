import {Block} from "~/interfaces/Block.interface";

export interface PageInfo {
  title: string;
  body: string;
  title_en: string;
  body_en: string;
  meta_description: string;
  meta_description_en: string;
  mode: string;
  created_at: string;
  updated_at: string;
  slug: string;
  parentId: string;
  announcement: AnnouncementInfo;
  cities: number[];
  blocks: Block[];
  uId: string;
  status?: number;
  options?: any;
}

export interface RecallFormResponse {

}

export interface ConsultFormResponse {}

/** Анонс Ленты */
export interface AnnouncementInfo {
  title: string;
  imgLink: string;
  tag: string;
}

/** описание данных в Меню */
export interface Menu {
  mode: string;
  createdDt: string;
  publishedDt: string;
  uId: string;
}

export interface PageProps {
  page: PageInfo;
  menu: Menu;
  preview?: boolean;
  error?: string;
}

export interface PageBlogProps extends PageProps {
  posts?: BlogItemProps[]
}

export interface BlogItemProps {
  access: boolean,
  canonical_url: string
  codeinjection_foot: string
  codeinjection_head: string
  comment_id: string
  created_at: string
  custom_excerpt: string
  custom_template: string
  email_subject: string
  excerpt: string
  feature_image: string
  feature_image_alt: string
  feature_image_caption: string
  featured: string
  frontmatter: string
  html: string
  id: string
  meta_description: string
  meta_title: string
  og_description: string
  og_image: string
  og_title: string
  published_at: string
  reading_time: number
  slug: string
  title: string
  twitter_description: string
  twitter_image: string
  twitter_title: string
  updated_at: string
  url: string
  uuid: string
  visibility: string
}

export interface BlocksRowProps {
  content: ContentInfo;
  blocks: Block[];
  noPadding?: boolean;
  rowsColors?: { [key: string]: number[] };
}


export interface ContentProps {
  children: React.ReactNode;
}

export interface ContentInfo {
  title?: string;
  body?: string;
}
