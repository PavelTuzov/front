/** Библиотеки */
import { useCallback, useContext, useEffect, useState } from 'react';
import { useMediaQuery } from 'react-responsive';

import * as React from 'react';

/** Компоненты */
//import { SidePage } from 'front.ui.lib';
import Content from './index';
//import GlobalPreloader from '../Preloader';
import AuthWizard from '../Wizards/AuthWizard';
import NotFound from './NotFound';

/** Контексты */
import { AuthContext } from '../Providers/AuthProvider';

/** Константы */
import { desktop940 } from '../Grid/constants';
import {
  ERROR_MESSAGE,
} from '../Wizards/AuthWizard/constants';

/** интерфейсы */
import { PageProps } from './types';
import { BlocksRow } from "~/components/Grid/BlocksRow";
import {useTranslation} from "next-i18next";
import { UsedLocales } from "front.ui.lib";
import { Loader } from "~/components/Loader/Loader";
import { PreloaderContext } from "~/components/Providers/PreloaderProvider";

/** Страница ленты */
const Page: React.FC<PageProps> = ({
  page,
  menu,
  preview
}: PageProps) => {

  const { i18n } = useTranslation('common');
  const lang = i18n.language;

  const {
    auth,
    toggleVisible: toggleAuthSidePage,
    isVisible: isAuthOpen,
    onCloseClickSidePageAuth,
  } = useContext(AuthContext);

  // Авторизация
  const isAuthorized = auth.id !== 0;

  const blocks = page.blocks;

  return (
    <Content>
      {blocks ? (
        <BlocksRow
          blocks={blocks}
          content={{title: lang === UsedLocales.ru ? page.title : page.title_en, body: lang === UsedLocales.ru ? page.body : page.body_en}}
          rowsColors={page?.options?.rowsColors}
        />
      ) : (
        <NotFound />
      )}
    </Content>
  );
};

export default React.memo(Page);
