/** Компонент-обёртка для формирования внутренней ссылки */
const NotFound: React.FC = () => (
  <>
    not found
  </>
);

export default NotFound;
