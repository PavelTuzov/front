import { useRouter } from "next/router";
import {FC, PropsWithChildren, useContext} from 'react';

import {StyledLayout} from './styles';
import {Header} from "~/components/Layout/Header/Header";
import * as React from "react";
import {Loader} from "~/components/Loader/Loader";
import {PreloaderContext} from "~/components/Providers/PreloaderProvider";

type MainLayoutProps = {
  /** Дополнительный класс для содержимого страницы */
  pageContentClassName?: string
};

export const MainLayout: FC<PropsWithChildren<MainLayoutProps>> = ({ pageContentClassName, children }) => {
  const route = useRouter();
  const { isLoaderVisible } = useContext(PreloaderContext);

  return (
    <>
      {isLoaderVisible && (<Loader />)}
      <Header/>
      <StyledLayout>
        {children}
      </StyledLayout>
    </>
  );
};
