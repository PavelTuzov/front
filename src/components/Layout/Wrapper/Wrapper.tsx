import {FC, PropsWithChildren} from 'react';
import { StyledWrapper } from "./styles";


export const Wrapper: FC<PropsWithChildren<any>> = ({children}) => {

  return (
    <StyledWrapper>
      {children}
    </StyledWrapper>
  );
};
