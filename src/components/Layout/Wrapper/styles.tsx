import styled from "@emotion/styled";
import {
  blockWidth280,
  blockWidth340, blockWidth400, desktop1100, desktop1536,
  desktop1280, desktop1440, desktop1920, desktop940,
  gap,
  minWidth
} from "~/components/Grid/constants";

export const StyledWrapper = styled.div`
  margin: 0 auto;
  position: relative;

  @media (min-width: ${desktop940}px) {
    max-width: 100%;
  }

  @media (min-width: ${desktop1280}px) {
    max-width: 1200px;
  }
  
  @media (min-width: ${desktop1440}px) {
    max-width: 1440px;

  }

  @media (min-width: ${desktop1920}px) {
    max-width: 1680px;
  }
`;
