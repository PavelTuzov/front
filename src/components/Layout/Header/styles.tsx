import styled from "@emotion/styled";
import { desktop940 } from "~/components/Grid/constants";
import { defaultTheme } from "front.ui.lib";

export const StyledNavIcon = styled.div<{ opened, onClick }>`
  ${({ opened }) => {
    return `
      user-select: none;
      max-width: 44px;
      min-width: 44px;
      height: 34px;
      position: relative;
      margin: 0px auto;
      -webkit-transform: rotate(0deg);
      -moz-transform: rotate(0deg);
      -o-transform: rotate(0deg);
      transform: rotate(0deg);
      -webkit-transition: .5s ease-in-out;
      -moz-transition: .5s ease-in-out;
      -o-transition: .5s ease-in-out;
      transition: .5s ease-in-out;
      cursor: pointer;
      flex: 1;
      
      & span {
        display: block;
        position: absolute;
        height: 2px;
        width: 100%;
        background: #333;
        border-radius: 1px;
        opacity: 1;
        left: 0;
        -webkit-transform: rotate(0deg);
        -moz-transform: rotate(0deg);
        -o-transform: rotate(0deg);
        transform: rotate(0deg);
        -webkit-transition: .25s ease-in-out;
        -moz-transition: .25s ease-in-out;
        -o-transition: .25s ease-in-out;
        transition: .25s ease-in-out;
        
        &:nth-of-type(1) {
          top: ${opened ? '1' : '0'}px;
          -webkit-transform-origin: left center;
          -moz-transform-origin: left center;
          -o-transform-origin: left center;
          transform-origin: left center;
          ${opened && '-webkit-transform: rotate(45deg)'};
          ${opened && '-moz-transform: rotate(45deg)'};
          ${opened && '-o-transform: rotate(45deg)'};
          ${opened && 'transform: rotate(45deg)'};
          ${opened && 'left: 8px'};
        }
    
        &:nth-of-type(2) {
          top: 16px;
          -webkit-transform-origin: left center;
          -moz-transform-origin: left center;
          -o-transform-origin: left center;
          transform-origin: left center;
          ${opened && 'width: 0%'};
          ${opened && 'opacity: 0'};
        }
        
        &:nth-of-type(3) {
          top: ${opened ? '32' : '32'}px;
          -webkit-transform-origin: left center;
          -moz-transform-origin: left center;
          -o-transform-origin: left center;
          transform-origin: left center;
          
          ${opened && '-webkit-transform: rotate(-45deg)'};
          ${opened && '-moz-transform: rotate(-45deg)'};
          ${opened && '-o-transform: rotate(-45deg)'};
          ${opened && 'transform: rotate(-45deg)'};
          ${opened && 'left: 8px'};
        }
      }
 
    `
  }}
`;
export const StyledHeaderContacts = styled.span`
  display: flex;
  align-items: center;
  & svg {
    max-width: 1.2rem;
    width: 1.2rem;
    fill: ${defaultTheme.colors.lightGrey};
    margin: 0 0.2rem 0 0;
  }
`;

export const StyledHeaderLanguage = styled.span`
  margin: 0;
`

export const StyledHeaderButton = styled.span`
  margin: 0;
`
export const StyledHeader = styled.header`
  padding: 0 1rem;
  position: sticky;
  top: 0;
  height: 60px;
  width: 100%;
  background: #fff;
  box-shadow: 0 0 10px rgba(0, 0, 0, 0.3);
  z-index: 3;

  transition-property: all;
  transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1);
  transition-duration: 500ms;
  &.hide {
    transform: translateY(-100%);
  }
  
  & .headerInner {
    display: flex;
    align-content: center;
    align-items: center;
    justify-content: space-between;
    height: 60px;
    gap: 1rem;
    &> * {
      
      &:first-of-type {
        margin-right: auto;
        margin-left: 0;
      }
    }
  }
`;

export const StyledButtonWrapper = styled.span`
  display: flex;
  flex: 5 1 0;
  justify-content: flex-end;
`

export const StyledMenuList = styled.ul`
  display: flex;
  padding: 0;
  margin: 0;
  list-style: none;
  & li {
    padding: 0 1rem;
    & a {
      display: block;
      font-size: 20px;
      transition: all 0.3s;
      color: ${defaultTheme.colors.lightBlack};
      text-decoration: none;
      &:hover {
        //text-decoration: underline;
      }
    }
  }

  @media (max-width: ${desktop940}px) {
    display: block;
    margin: 0 0 1rem 0;
    padding: 0;
    & li {
      display: block;
      padding: 0;
      & a {
        padding: 0 0 1rem 0;
        font-size: 26px;
      }
    }
  }
`;

export const StyledMenuListMobileWrapper = styled.div<{show?: boolean}>`
  ${({
     theme,
     show,
  }) => {
    return `

      padding: 2rem;
      display: block;
      position: fixed;
      top: 60px;
      bottom: 0;
      right: 0;
      z-index: 10;
      width: 100%;
      background: rgba(255, 255, 255, 0.7);
      backdrop-filter: blur(7px);
    `
  }}
`;


export const TransitionMenuWrapper = styled.div`
  ${({ theme }) => {
  return `
  .enter {
    animation: slideIn 500ms ease;
  }

  .exit {
    animation: slideOut 500ms ease;
  }

  @keyframes slideIn {
    0% {
      transform: translateX(100%);
    }
    100% {
      transform: translateX(0);
    }
  }

  @keyframes slideOut {
    0% {
      transform: translateX(0);
    }
    100% {
      transform: translateX(100%);
    }
  }
  `;
}}
`;

