import { FC, useEffect, useState  } from 'react';
import {
  StyledButtonWrapper,
  StyledHeader,
  StyledHeaderButton,
  StyledHeaderContacts,
  StyledHeaderLanguage,
  StyledMenuList,
  StyledMenuListMobileWrapper,
  StyledNavIcon,
  TransitionMenuWrapper
} from "~/components/Layout/Header/styles";
import Link from "next/dist/client/link";
import { Language } from "~/components/Language/Language";
import { Button, ButtonSizeTypes, ButtonStyleTypes, SidePage} from 'front.ui.lib';
import { Wrapper } from "~/components/Layout/Wrapper/Wrapper";
import { useTranslation } from "next-i18next";
import { useMediaQuery } from 'react-responsive';
import { desktop940 } from "~/components/Grid/constants";
import { CSSTransition } from 'react-transition-group';
import { StyledLogo } from "~/components/Logo/styles";
import { RecallForm } from "~/components/Forms/RecallForm/RecallForm";
import { PhoneIcon } from "~/components/icons/PhoneIcon";
import ResultForm from "~/components/Forms/ResultForm/ResultForm";
import { FormResult } from "~/components/Forms/ResultForm/interfaces";
import Portal from "~/components/Portal/Portal";
import { useScrollDirection } from "~/hooks/useScrollDirectionHook";

export const Header: FC = () => {
  const { t } = useTranslation('common');
  const { scrollDirection, scrollPosition } = useScrollDirection();
  const isMobile = useMediaQuery({
    query: `(max-width: ${desktop940}px)`,
  });

  const menu = [
    {
      title: t('nav.services'),
      href: 'services',
    },
    {
      title: t('nav.advantages'),
      href: 'advantages'
    },
    {
      title: t('nav.blog'),
      href: 'blog'
    },
    {
      title: t('nav.contacts'),
      href: 'contacts'
    },
  ];

  const [isFormOpen, setIsFormOpen] = useState<boolean>(false);
  const [isMenuOpen, setIsMenuOpen] = useState<boolean>(false);
  const [isFormFinished, setIsFormFinished] = useState<boolean>(false);

  const handleRecallFormFinish = () => {
    setIsFormFinished(true);
  }

  useEffect(() => {
    setTimeout(() => {
      setIsFormFinished(false);
    }, 300)
  }, [isFormOpen])

  return (
    <StyledHeader className={scrollDirection === 'down' && scrollPosition > 100 ? 'hide' : 'show'}>
      <Wrapper>
        <div className={'headerInner'}>
          <Link href="/">
            <a style={{'textDecoration': 'none'}}>
              <StyledLogo>ptuzov</StyledLogo>
            </a>
          </Link>

          {isMobile ? (
            <>
              <TransitionMenuWrapper>
                <CSSTransition
                  in={isMenuOpen}
                  timeout={500}
                  classNames={{
                    enterActive: 'enter',
                    exitActive: 'exit',
                  }}
                  unmountOnExit
                >
                  <StyledMenuListMobileWrapper>
                    <StyledMenuList>
                      {menu.map((item, k) => <li onClick={() => setIsMenuOpen(false)} key={k}><Link href={item.href}><a>{item.title}</a></Link></li>)}
                    </StyledMenuList>
                    <Language />
                  </StyledMenuListMobileWrapper>
                </CSSTransition>
              </TransitionMenuWrapper>

              <StyledButtonWrapper>
                <Button
                  onClick={() => setIsFormOpen(!isFormOpen)}
                  styleSize={ButtonSizeTypes.SMALL}
                  tabIndex={0}
                  styleType={ButtonStyleTypes.SECONDARY}
                >
                  {t('makeOrder')}
                </Button>
              </StyledButtonWrapper>
              <StyledNavIcon opened={isMenuOpen} onClick={() => setIsMenuOpen(!isMenuOpen)}>
                <span></span>
                <span></span>
                <span></span>
              </StyledNavIcon>
            </>
          ) : (
            <>
              <StyledMenuList>
                {menu.map((item, k) => <li key={k}><Link href={item.href}><a>{item.title}</a></Link></li>)}
              </StyledMenuList>

              <StyledHeaderContacts>
                <PhoneIcon/>+7 343 000-00-00
              </StyledHeaderContacts>
              <StyledHeaderLanguage>
                <Language />
              </StyledHeaderLanguage>
              <StyledHeaderButton>
                <Button onClick={() => setIsFormOpen(!isFormOpen)} styleSize={ButtonSizeTypes.SMALL} tabIndex={0}>{t('recall')}</Button>
              </StyledHeaderButton>
            </>
          )}

        </div>
        <Portal>
          <SidePage
            show={isFormOpen}
            onCloseClick={() => setIsFormOpen(false)}
            headerText={isFormFinished ? null : t('recallForm.title')}
          >
            {isFormFinished ? (
              <ResultForm title={t('form.success')} result={FormResult.SUCCESS} />
            ) : (
              <RecallForm onFinish={handleRecallFormFinish}/>
            )}
          </SidePage>
        </Portal>
      </Wrapper>
    </StyledHeader>
  );
};
