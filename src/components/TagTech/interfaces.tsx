import { ReactNode } from "react";

export interface TagProps {
  id: number,
  title: string,
  color?: string,
  logo: string,
  onClick?: () => void,
  body?: string,
  body_en?: string,
}