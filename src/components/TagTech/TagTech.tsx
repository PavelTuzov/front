import React, {FC} from 'react';
import { TagProps } from "./interfaces";
import { StyledTagTech } from "~/components/TagTech/styles";
import { MAP_ICONS } from "~/components/Blocks/Templates/TagLine/constants";


const TagTech:FC<TagProps> = ({color, logo, title, onClick}) => (
  <StyledTagTech
    onClick={onClick}
    className='tag'
    style={{
      borderColor: '#'+color,
      color: '#'+color
    }}
  >
    <span>{MAP_ICONS[logo]}</span>
    {title}
  </StyledTagTech>
);


export default TagTech;