import styled from "@emotion/styled";

export const StyledTagTech = styled.span`
  display: flex;
  align-items: center;
  gap: 0 0.2rem;
  color: #334155;
  font-size: 0.9rem;
  border: 2px solid #fff;
  border-radius: 0.4rem;
  padding: 0.7rem 1rem;
  margin-right: 1rem;
  cursor: pointer;

  span {
    font-size: 1.2rem;
    color: #64748b;
    & svg {
      max-width: 1rem;
      max-height: 1rem;
    }
  }

  &:hover {
    box-shadow: 0 0 5px rgba(0, 0, 0, 0.3);
  }
`;