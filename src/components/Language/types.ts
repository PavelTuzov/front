export enum SupportedLanguage {
  Ru = 'ru',
  En = 'en',
}
