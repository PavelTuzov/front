import styled from "@emotion/styled";

export const StyledLangList = styled.ul`
  list-style: none;
  & li {
    display: block;
  }
`;