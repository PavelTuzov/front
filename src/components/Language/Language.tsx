import { FC, useEffect, useState } from 'react';
import { SupportedLanguage } from "~/components/Language/types";
import { RuFlag, UsFlag } from "~/components/icons";
import { useRouter } from "next/dist/client/router";
import { DropDown, DropDownItemProps, DropDownSizeTypes } from 'front.ui.lib';
import { useMediaQuery } from 'react-responsive';

import { desktop1280 } from "~/components/Grid/constants";

const supportedLanguageOptions = [
  {
    code: SupportedLanguage.Ru,
    name: 'По-русски',
    flag: <RuFlag />,
  },
  {
    code: SupportedLanguage.En,
    name: 'In english',
    flag: <UsFlag />,
  },
];

export const Language: FC = () => {
  const router = useRouter();
  const [langList, setLangList] = useState<DropDownItemProps[]>()
  const [langItem, setLangItem] = useState<DropDownItemProps>()
  const isSmall = useMediaQuery({
    query: `(max-width: ${desktop1280}px)`,
  });

  const isLarge = useMediaQuery({
    query: `(min-width: ${desktop1280}px)`,
  });

  const getValue = (value: string) => {
    if (isSmall) {
      return null;
    }

    if (isLarge) {
      return value;
    }

    return value;
  }

  useEffect(() => {

    const langs = supportedLanguageOptions.map((lang) => {
      const item = {
        key: lang.code,
        value: getValue(lang.name),
        icon: lang.flag
      };

      if (lang.code === router.locale) {
        setLangItem(item);
      }

      return item;
    });

    setLangList(langs);
  }, []);

  const handleChange = async (item: DropDownItemProps) => {
    const { pathname, asPath, query } = router;
    await router.push({ pathname, query }, asPath, { locale: item.key, scroll:false });
  }

  return (
    <>
      {langItem && (
        <DropDown
          options={langList}
          defaultOption={langItem}
          handleChange={handleChange}
          tabIndex={0}
          styleSize={DropDownSizeTypes.SMALL}
        />
      )}
    </>
  );
};
