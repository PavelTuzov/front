import * as React from 'react';
import { defaultTheme } from 'front.ui.lib';
import { ThemeProvider } from '@emotion/react';
import GlobalStyles from "../GlobalStyles";
import Seo from './Seo.provider';
import AuthProvider from './AuthProvider';
import { ProviderData } from './ProviderData.types';
import PreloaderProvider from './PreloaderProvider';

interface Props {
  providerData?: ProviderData;
  children?: any;
}

const MainProvider = ({
  providerData,
  children,
}: Props): JSX.Element => {
  const [mounted, setMounted] = React.useState(false);

  React.useEffect(() => {
    setMounted(true);
  }, []);

  const body = (
    <ThemeProvider theme={defaultTheme}>
      <Seo description={'asd'} title={'asd'}/>
        <GlobalStyles />
        <PreloaderProvider>
          <AuthProvider>
            {children}
          </AuthProvider>
        </PreloaderProvider>
    </ThemeProvider>
  );

  // prevents ssr flash for mismatched dark mode
  if (!mounted) {
    return <div style={{ visibility: 'hidden' }}>Loading...</div>;
  }

  return body;
};

export default MainProvider;
