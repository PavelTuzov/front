/** библиотеки */
import { useState, useEffect, Dispatch, SetStateAction } from 'react';

import * as React from 'react';

/** api */
import {
  cookies,
  checkAuth,
  getUserInfo,
} from '../../api/api';

/** константы */
import {
  TOKEN,
  DEFAULT_CONTRACT_AUTH,
  CONTRACT_AUTH,
  COOKIES_DEAD_TIME_IN_MINUTES,
} from '../../constants/common';

/** интерфейсы */
import { Auth } from '../../interfaces/Auth.interface';
import { HooksTyping } from '../../utils/typeScriptHelper';
import useMakeAuth from "../Wizards/AuthWizard/AuthWizardHooks";

/** интерфейс авторизационного контекста */
interface AuthContext {
  auth: Auth;
  setAuth: Dispatch<SetStateAction<Auth>>;
  createAuth: Dispatch<SetStateAction<Auth>>;
  removeAuth: () => void;
  errorLogin: boolean;
  errorPassword: boolean;
  login: string;
  password: string;
  setLogin: Dispatch<SetStateAction<string>>;
  setPassword: Dispatch<SetStateAction<string>>;
  setErrorLogin: Dispatch<SetStateAction<boolean>>;
  setErrorPassword: Dispatch<SetStateAction<boolean>>;
  isLoading: boolean;
  isCompleted: boolean;
  makeAuth: () => Promise<void>;
  toggleVisible: () => void;
  isVisible: boolean;
  onCloseClickSidePageAuth: () => void;
  validatePassword: () => void;
  validateLogin: () => void;
  serverError?: string;
  makeAuthData?: Auth;
  isAuth: any;
}

/** кастомный хук авторизационного контекста */
const useIsAuthHandler = (initialState: Auth) => {
  const [currentSeries, setCurrentSeries] = useState(null);
  const [auth, setAuth]: HooksTyping<Auth> = useState<Auth>(
    initialState,
  );
  /**
   * Сохраняет в cookies и в контекст данные авторизации
   * @param {Auth} data
   */
  const createAuth = async (data: Auth) => {
    if (!data.token) {
      removeAuth();
      return;
    }
    const cookiesDeadTime: Date = new Date();
    const cookiesDomain =
      window.location.hostname !== 'localhost'
        ? process.env.COOKIE_DOMAIN
        : null;
    const cookieOptions = {
      path: '/',
      expires: cookiesDeadTime,
      domain: cookiesDomain,
    };

    cookiesDeadTime.setMinutes(
      cookiesDeadTime.getMinutes() + COOKIES_DEAD_TIME_IN_MINUTES,
    );

    cookies.set(TOKEN, data.token, cookieOptions);
    try {
      const {
        name,
        id,
        cityId,
      } = await getUserInfo();

      cookies.set(
        CONTRACT_AUTH,
        {
          name,
          id,
          cityId,
        },
        cookieOptions,
      );
      setAuth({
        name,
        id,
        cityId,
        token: data.token,
      });
    } catch (e) {
      console.error(e);
      removeAuth();
    }
  };

  /** Удаляет из cookies и из контекста данные авторизации */
  const removeAuth = () => {
    cookies.remove(TOKEN, { path: '/' });
    cookies.remove(CONTRACT_AUTH, { path: '/' });
    setAuth(DEFAULT_CONTRACT_AUTH);
  };

  /** Флаг авторизации */
  const isAuth = auth.id;

  return {
    currentSeries,
    auth,
    setCurrentSeries,
    setAuth,
    createAuth,
    removeAuth,
    isAuth,
  };
};

/** авторизационный контекст */
export const AuthContext = React.createContext<AuthContext>({
  auth: DEFAULT_CONTRACT_AUTH,

  setAuth: () => {
    /* context init */
  },

  createAuth: () => {
    /* context init */
  },
  removeAuth: () => {
    /* context init */
  },

  login: '',
  errorLogin: false,
  errorPassword: false,
  password: '',
  isLoading: false,
  isCompleted: true,
  isVisible: false,
  serverError: null,
  makeAuthData: null,
  setLogin: () => {
    /* context init */
  },
  setPassword: () => {
    /* context init */
  },
  setErrorLogin: () => {
    /* context init */
  },
  setErrorPassword: () => {
    /* context init */
  },
  makeAuth: async () => {
    /* context init */
  },
  toggleVisible: () => {
    /* context init */
  },
  onCloseClickSidePageAuth: () => {
    /* context init */
  },
  validateLogin: () => {
    /* context init */
  },
  validatePassword: () => {
    /* context init */
  },
  isAuth: false,
});

const { Provider } = AuthContext;

/**
 * Провайдер авторизационного контекста
 * @param children
 */
const AuthProvider: React.FC<{ children: React.ReactNode }> = ({
    children,
  }) => {
  const currentToken = cookies.get(TOKEN);
  const contractAuth = cookies.get(CONTRACT_AUTH);
  let currentContractAuth: Auth;
  if (contractAuth) {
    const {
      id,
      name,
      cityId,
    } = contractAuth as Auth;
    currentContractAuth = {
      id,
      name,
      cityId,
      token: currentToken,
    };
  } else {
    currentContractAuth = DEFAULT_CONTRACT_AUTH;
  }
  const {
    auth,
    setAuth,
    createAuth,
    removeAuth,
    isAuth,
  } = useIsAuthHandler(currentContractAuth);

  const {
    login,
    password,
    errorLogin,
    errorPassword,
    setPassword,
    setLogin,
    setErrorLogin,
    setErrorPassword,
    isLoading,
    isCompleted,
    makeAuth,
    toggleVisible,
    isVisible,
    onCloseClickSidePageAuth,
    validatePassword,
    validateLogin,
    makeAuthData,
  } = useMakeAuth();

  /**
   * Проверяет авторизационный token на backend'e
   */
  const checkAuthAndSetContractData = async () => {
    if (currentToken) {
      try {
        const res = await checkAuth();
        if (res.status === 200) await updateContractData();
      } catch (e) {
        console.error(e);
        removeAuth();
      }
    } else {
      removeAuth();
    }
  };


  /**
   * Обновляет данные договора
   */
  const updateContractData = async () => {
    if (!currentToken) return;
    const cookiesDeadTime: Date = new Date();
    cookiesDeadTime.setMinutes(
      cookiesDeadTime.getMinutes() + COOKIES_DEAD_TIME_IN_MINUTES,
    );
    try {
      const {
        id,
        name,
        cityId,
      } = await getUserInfo();
      cookies.remove(CONTRACT_AUTH, { path: '/' });
      cookies.set(
        CONTRACT_AUTH,
        {
          name,
          id,
          cityId,
        },
        { path: '/', expires: cookiesDeadTime },
      );
      setAuth({
        name,
        id,
        cityId,
        token: currentToken,
      });
    } catch (e) {
      console.error(e);
      removeAuth();
    }
  };

  useEffect(() => {
    if (makeAuthData) createAuth(makeAuthData);
  }, [makeAuthData]);

  useEffect(() => {
    checkAuthAndSetContractData();
  }, []);

  return (
    <Provider
      value={{
        auth,
        setAuth,
        createAuth,
        removeAuth,
        login,
        errorLogin,
        errorPassword,
        password,
        setPassword,
        setLogin,
        setErrorLogin,
        setErrorPassword,
        isLoading,
        isCompleted,
        makeAuth,
        toggleVisible,
        isVisible,
        onCloseClickSidePageAuth,
        validatePassword,
        validateLogin,
        isAuth,
      }}
    >
      {children}
    </Provider>
  );
};

export default AuthProvider;
