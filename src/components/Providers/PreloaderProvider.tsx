/* eslint-disable prettier/prettier */
/** библиотеки */
import { useState, useEffect } from 'react';

import * as React from 'react';
import Router from 'next/router';

/** интерфейс контекста городов */
interface PreloaderContext {
  isLoaderVisible: boolean;
}

/** контекст городов */
export const PreloaderContext = React.createContext<PreloaderContext>({
  isLoaderVisible: false,
});

const { Provider } = PreloaderContext;
/**
 * Провайдер контекста городов
 * @param cities
 * @param children
 */
const PreloaderProvider: React.FC<{ children: React.ReactNode }> = ({
                                                                      children,
                                                                    }) => {
  const [isVisible, setIsVisible] = useState(false);

  const routeChangeStart = (url) => {
    setIsVisible(true);
  };

  const routeChangeEnd = (url) => {
    setIsVisible(false);
  };

  useEffect(() => {
    Router.events.on('routeChangeStart', routeChangeStart);
    Router.events.on('routeChangeComplete', routeChangeEnd);
    Router.events.on('routeChangeError', routeChangeEnd);

    return () => {
      Router.events.off('routeChangeStart', routeChangeStart);
      Router.events.off('routeChangeComplete', routeChangeEnd);
      Router.events.off('routeChangeError', routeChangeEnd);
    };
  });

  return (
    <Provider value={{ isLoaderVisible: isVisible }}>{children}</Provider>
  );
};

export default PreloaderProvider;
