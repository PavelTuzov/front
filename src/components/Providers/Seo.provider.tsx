import Head from 'next/head';
import { DefaultSeo } from 'next-seo';

const SeoConfig = {
  title: 'tmp',
  description: 'tmp description',
  keywords: 'tmp keywords'
};

interface SeoProps {
  title: string;
  description: string;
}

const SeoProvider = ({ title, description }: SeoProps): JSX.Element => {

  return (
    <>
      <DefaultSeo
        title={title ?? SeoConfig.title}
        description={description ?? SeoConfig.description}
      />
      <Head>
        <></>
      </Head>
    </>
  );
};

export default SeoProvider;
