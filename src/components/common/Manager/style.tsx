import styled from "@emotion/styled";

export const StyledManagerWrapper = styled.div`
  ${({ theme }: any) => {
    return `
      width: 100%;
      height: 100%;
      display: flex;
      align-items: center;
      align-content: center;
      justify-content: flex-start;
      background: ${theme.colors.white};
      transition: all 0.3s;
      padding: 1rem;
      margin: 0;
    `
  }}
`;

export const StyledManagerContent = styled.div`
  ${({ theme }: any) => {
  return `
    padding: 2rem;
    background: ${theme.colors.accent};
    border-radius: 24px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    height: calc(100% - 1rem);
    color: ${theme.colors.whiteSmoke};
    width: 100%;
    transition: all 0.3s;
    margin: 0;
    `
}}
`;

export const StyledManagerImage = styled.img`
  border-radius: 20px;
  width: 100px;
  height: 100px;
  margin: 0 0 2rem 0;
`;

export const StyledManagerName = styled.h3`
  margin: 0;
  font-size: 0.9rem;
`;

export const StyledManagerPosition = styled.div`
  margin: 0;
  font-size: 0.9rem;
  opacity: 0.6;
`;

export const StyledManagerQuote = styled.div`
  ${({ theme }: any) => {
    return `
      font-size: 1.4rem;
      line-height: 2rem;
      padding: 2rem;
      margin: 0 0 2rem 0;
      background: ${theme.colors.accentDark};
      position: relative;
      color: #fff;
      border-radius: 20px;

      &::before, &::after {
        font-size: 5rem;
        position: absolute;
        opacity: 0.2;
        display: inline-block;
      }
      &::before {
        content: '“';
        left: 0;
        top: 0;
      }
      &::after {
        content: '”';
        right: 0;
        bottom: -2rem;
      }
    `
  }}
`;
