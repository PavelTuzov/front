import { FC, useEffect, useState } from 'react';
import { StyledManagerContent, StyledManagerWrapper, StyledManagerImage, StyledManagerQuote, StyledManagerPosition, StyledManagerName } from "./style";
import { Loader as Load, UsedLocales } from 'front.ui.lib';
import { getManagers } from "~/api/api";
import { ManagerInfo, ManagerListResponse, ManagerProps } from "./types";
import { useTranslation } from "next-i18next";

export const Manager: FC<ManagerProps> = ({ selectedDepartment, setShowManagerCol }: ManagerProps) => {
  const { t, i18n } = useTranslation('common');
  const lang = i18n.language;

  const [managers, setManagers] = useState<ManagerListResponse>(undefined);
  const [manager, setManager] = useState<ManagerInfo>();
  const [isLoading, setIsLoading] = useState<boolean>(true);
  useEffect(() => {
    setManager(null);
    const fetchManagersData = async () => {
      const result = await getManagers();
      setIsLoading(false);
      setManagers(result);
    }

    fetchManagersData()
      .catch(console.error)
  }, []);

  useEffect(() => {
    if (!managers) return;

    const resultManager = managers.find(item => item.departments.some(d => +d.id === +selectedDepartment));
    setManager(resultManager);
    setShowManagerCol(!!resultManager);
  }, [selectedDepartment, managers])

  return (
    <StyledManagerWrapper>
      {isLoading && <Load />}
      {(manager) ? (
        <StyledManagerContent>
          <StyledManagerImage src={manager.photo} />
          <StyledManagerQuote>{lang === UsedLocales.ru ? manager.quote : manager.quote_en}</StyledManagerQuote>
          <StyledManagerName>{lang === UsedLocales.ru ? manager.name : manager.name_en}</StyledManagerName>
          <StyledManagerPosition>{lang === UsedLocales.ru ? manager.position : manager.position_en}</StyledManagerPosition>
        </StyledManagerContent>
      ) : <></>}
    </StyledManagerWrapper>
  );
};