import {TagProps} from "~/components/TagTech/interfaces";

export type ManagerListResponse = ManagerInfo[];

export type ManagerInfo = {
  id: number
  name: string
  position: string
  name_en: string
  position_en: string
  quote: string
  quote_en: string
  photo: string
  is_online: number
  created_at: string
  updated_at: string
  departments: DepartmentInfo[]
}

export interface ManagerProps {
  selectedDepartment: number
  setShowManagerCol: (boolean) => void
}

export type DepartmentListResponse = DepartmentInfo[];
export type TagsListResponse = TagProps[]

export type DepartmentInfo = {
  id: number
  title: string
  title_en: string
}