import styled from "@emotion/styled";
import { GradientTextProps } from "~/components/common/GradientText/interfaces";
import {desktop940} from "~/components/Grid/constants";

export const StyledGradientText = styled.span<GradientTextProps>`
  ${({ theme, size = 1, lineHeight = size + size / 1.5, bold = false }: any) => {
    return `
      font-weight: ${bold ? 'bold' : 'normal'};
      font-size: ${size}rem;
      line-height: ${lineHeight}rem;
      background: -webkit-linear-gradient(left, ${theme.colors.mainDark}, ${theme.colors.accentDark});
      background: -o-linear-gradient(right, ${theme.colors.mainDark}, ${theme.colors.accentDark});
      background: -moz-linear-gradient(right, ${theme.colors.mainDark}, ${theme.colors.accentDark});
      background: linear-gradient(to right, ${theme.colors.mainDark}, ${theme.colors.accentDark});
      -webkit-background-clip: text;
      -webkit-text-fill-color: transparent;
      
      @media (max-width: ${desktop940}px) {
        font-size: ${size / 1.2}rem;
        line-height: ${lineHeight / 1.2}rem;
      }
    `
  }}
`;