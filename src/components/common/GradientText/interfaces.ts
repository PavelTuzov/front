export interface GradientTextProps {
  size?: number,
  lineHeight?: number,
  bold?: boolean
}