import { FC } from 'react';
import ResetStyles from './reset';

const GlobalStyles: FC = () => (
  <>
    <ResetStyles />
  </>
);

export default GlobalStyles;
