import { FC } from 'react';
import { css, Global } from '@emotion/react';
import { defaultTheme } from "front.ui.lib";


const ResetStyles: FC = () => {
  const theme = defaultTheme;
  return (
    <Global
      styles={css`
        a {
          display: inline-block;
          position: relative;

        }

        a::before {  
          transform: scaleX(0);
          transform-origin: bottom right;
        }
        
        a:hover::before {
          transform: scaleX(1);
          transform-origin: bottom left;
        }
        
        a::before {
          content: " ";
          display: block;
          position: absolute;
          top: 0; right: 0; bottom: 0; left: 0;
          inset: 0 0 0 0;
          background: ${defaultTheme.colors.mainLight};\
          z-index: -1;
          transition: transform .3s ease;
        }
        
        a.noEffect::before {
          display: none;
          transform: none;
        }
        
        a.noEffect:hover::before {
          display: none;
          transform: none;
        }

        *,
        :before,
        :after {
          box-sizing: border-box;
          -webkit-user-drag: none;
          outline: none;
        }
        ::selection {
          color: ${defaultTheme.colors.disable};
          background-color: ${defaultTheme.colors.main};
        }
        html {
          display: flex;
          height: 100%;
          width: 100%;
          max-height: 100%;
          max-width: 100%;
          box-sizing: border-box;
          font-family: Inter, sans-serif;
          font-size: ${theme.common.fontSize};
          color: ${theme.colors.lightBlack};
          background-color: ${theme.common.backgroundColor};
          padding: 0;
          margin: 0;
        }
        body {
          margin: 0;
          width: 100%;
          height: 100%;
        }
        sup {
          font-size: 10px;
          line-height: 0;
        }
        p {
          font-size: 16px;
          line-height: 22px;
          font-weight: 400;
        }
      `}
    />
  );
};

export default ResetStyles;
