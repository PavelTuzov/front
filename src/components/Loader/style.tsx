import styled from "@emotion/styled";

export const StyledLoaderWrapper = styled.div`
  position: fixed;
  z-index: 1000;
  width: 100%;
  height: 100vh;
  display: flex;
  align-items: center;
  align-content: center;
  justify-content: center;
  background: rgba(0, 0, 0, 0.5);
  transition: all 0.3s;
`;