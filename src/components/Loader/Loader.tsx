import { FC } from 'react';
import { StyledLoaderWrapper } from "~/components/Loader/style";
import {Loader as Load} from 'front.ui.lib';
export const Loader: FC = () => {
  return (
    <StyledLoaderWrapper>
      <Load />
    </StyledLoaderWrapper>
  );
};