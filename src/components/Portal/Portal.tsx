import { createPortal } from 'react-dom';

function Portal({ children, wrapperId = 'portal' }) {
  const mount = document.getElementById(wrapperId);

  return createPortal(children, mount);
}

export default Portal;
