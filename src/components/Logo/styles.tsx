import styled from "@emotion/styled";
import {desktop940} from "~/components/Grid/constants";

export const StyledLogo = styled.span`
  flex: 2;
  font-weight: bold;
  font-size: 1.3rem;
  text-transform: uppercase;
  color: #fff;
  background: #52b0d9;
  padding: 0.2rem 0.5rem;
  border-radius: 4px;
  display: flex;
  align-items: center;

  & span {
    font-size: 2rem;
    padding: 0 0.2rem;
    font-weight: bold;
  }

  @media (max-width: ${desktop940}px) {
    font-size: 1.1rem;
  }
`;