import { HTMLProps } from "react";

/** Props компонента Row */
export interface RowProps extends HTMLProps<HTMLDivElement> {
  /** без отступа */
  noPadding?: boolean;
  /** цвет фона */
  backgroundColor?: string;
}

export interface RowStyledProps extends RowProps {
  paddingBetweenRows: string
  paddingNextToRows: string
  inView: boolean
}