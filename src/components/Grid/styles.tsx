import styled from "@emotion/styled";
import {
  blockWidth280,
  blockWidth340, blockWidth400, desktop1100,
  desktop1280, desktop1440, desktop1920, desktop940,
  gap,
  minWidth
} from "~/components/Grid/constants";
import {RowProps, RowStyledProps} from "~/components/Grid/interfaces";


/**
 * Контейнер для отображения компонентов Row
 */
export const StyledRow = styled.div<RowStyledProps>`
  ${({ backgroundColor, paddingNextToRows, paddingBetweenRows, noPadding, inView }: any) => {
    return `
      transition: all 0.5s;
      transform: scale(${inView ? 1 : 0.9});
      opacity: ${inView ? 1 : 0.5};
      display: block;
      padding: ${paddingBetweenRows} ${noPadding ? 0 : paddingNextToRows};
      width: 100vw;
      background-color: ${backgroundColor};
      min-height: auto;
    
      &:first-of-type {
        padding-top: 2rem;
      }
    
      @media (min-width: ${desktop940}px) {
        display: flex;
        justify-content: center;
      }
    `}
  }
`;


export const StyledCol = styled.div`
  flex: 0 0 0 auto;
  border-radius: 0;
  width: 100%;
  min-width: ${minWidth}px;
  padding: 0;
  &.col-2, &.col-3, &.col-4 {
    max-width: 100%;
  }

  & + & {
    margin-top: ${gap}px;
  }
  
  @media (min-width: ${desktop940}px) {
    width: ${blockWidth280}px;
    min-width: unset;

    &.col-2 {
      width: ${blockWidth280 * 2 + gap}px;
    }

    &.col-3 {
      width: ${blockWidth280 * 3 + gap * 2}px;
    }

    &.col-4 {
      width: ${blockWidth280 * 4 + gap * 3}px;
    }
    
    & + & {
      margin-left: ${gap}px;
      margin-top: 0;
    }
  }

  @media (min-width: ${desktop1280}px) {
    width: ${blockWidth280}px;
    min-width: unset;
    max-width: none;

    &.col-2 {
      width: ${blockWidth280 * 2 + gap}px;
    }

    &.col-3 {
      width: ${blockWidth280 * 3 + gap * 2}px;
    }

    &.col-4 {
      width: ${blockWidth280 * 4 + gap * 3}px;
    }
  }

  @media (min-width: ${desktop1440}px) {
    width: ${blockWidth340}px;


    &.col-2 {
      width: ${blockWidth340 * 2 + gap}px;
    }

    &.col-3 {
      width: ${blockWidth340 * 3 + gap * 2}px;
    }

    &.col-4 {
      width: ${blockWidth340 * 4 + gap * 3}px;
    }
  }

  @media (min-width: ${desktop1920}px) {
    width: ${blockWidth400}px;

    &.col-2 {
      width: ${blockWidth400 * 2 + gap}px;
    }

    &.col-3 {
      width: ${blockWidth400 * 3 + gap * 2}px;
    }

    &.col-4 {
      width: ${blockWidth400 * 4 + gap * 3}px;
    }
  }
`;