import { RowProps } from "~/components/Grid/interfaces";
import { FC, PropsWithChildren } from "react";
import { StyledRow } from "./styles";
import { useInView } from 'react-intersection-observer';

const paddingNextToRows = '20px';
const paddingBetweenRows = '16px';

export const Row:FC<PropsWithChildren<RowProps>> = (props: RowProps) => {
  const { children } = props;
  const { ref, inView, entry } = useInView({
    /* Optional options */
    threshold: 0.1,
  });

  return (
    <StyledRow
      paddingBetweenRows={paddingBetweenRows}
      paddingNextToRows={paddingNextToRows}
      backgroundColor={props.backgroundColor}
      noPadding={props.noPadding}
      inView={inView}
      ref={ref}
    >
      {children}
    </StyledRow>
  );
};


