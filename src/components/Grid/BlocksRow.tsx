import * as React from 'react';
import { Row } from '~/components/Grid/Row';
import { Col } from '~/components/Grid/Col';
import { BlocksRowProps } from "~/components/Page/types";
import BlockSelector from "~/components/Blocks/BlockSelector";
import { maxSize } from "~/components/Grid/constants";

/**
 * @param blocks блоки
 * @param noPadding без отступа
 * @param rowsColors цвет фона rows
 * @param page
 */
export const BlocksRow: React.FC<BlocksRowProps> = ({
  blocks,
  noPadding = false,
  rowsColors = {},
  content
}: BlocksRowProps) => {
  const rows = [];

  /** цвет фона row */
  const getRowColor = () => {
    const index = Object.values(rowsColors).findIndex((item) =>
      item.includes(rows.length + 1),
    );
    if (index === -1) return '';
    return Object.keys(rowsColors)[index];
  };

  for (let i = 0; i < blocks.length; i += 1) {
    const { size } = blocks[i];
    const { size: size2 } = blocks[i + maxSize - 3] || {};
    const { size: size3 } = blocks[i + maxSize - 2] || {};
    const { size: size4 } = blocks[i + maxSize - 1] || {};
    const isRenderBlock2 = size + size2 <= maxSize;
    const isRenderBlock3 = size + size2 + size3 <= maxSize;
    const isRenderBlock4 = size + size2 + size3 + size4 <= maxSize;

    rows.push(
      <Row
        className="row"
        noPadding={noPadding}
        key={i}
        backgroundColor={getRowColor()}
      >
        <Col size={size}>
          <BlockSelector key={i} content={content} block={blocks[i]} />
        </Col>
        {isRenderBlock2 && (
          <Col size={size2}>
            <BlockSelector key={i + 1} content={content} block={blocks[i + 1]} />
          </Col>
        )}
        {isRenderBlock3 && (
          <Col size={size3}>
            <BlockSelector key={i + 2} content={content} block={blocks[i + 2]} />
          </Col>
        )}
        {isRenderBlock4 && (
          <Col size={size4}>
            <BlockSelector key={i + 3} content={content} block={blocks[i + 3]} />
          </Col>
        )}
      </Row>,
    );
    if (isRenderBlock4) i += 3;
    else if (isRenderBlock3) i += 2;
    else if (isRenderBlock2) i += 1;
  }
  return <>{rows}</>;
};
