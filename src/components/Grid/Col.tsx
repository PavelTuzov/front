/** Библиотеки */
import * as React from 'react';

/* Стили */
import { StyledCol } from "~/components/Grid/styles";

/** Props компонента Col */
interface ColProps extends React.HTMLProps<HTMLDivElement> {
  /** Размер блока */
  size?: 1 | 2 | 3 | 4;
  children?: JSX.Element;
}

export const Col: React.FC<ColProps> = ({ size = 1, children }: ColProps) => {
  const style = `col-${size}`;
  return <StyledCol className={style}>{children}</StyledCol>;
};
