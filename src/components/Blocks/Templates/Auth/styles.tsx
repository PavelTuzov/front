import styled from "@emotion/styled";

export const StyledBlock = styled.div`
  background: #eee;
  padding: 2rem;
`;