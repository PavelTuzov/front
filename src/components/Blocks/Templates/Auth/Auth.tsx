/** Библиотеки */
import * as React from 'react';
import {FC} from "react";
import { StyledBlock } from "~/components/Blocks/Templates/Auth/styles";


export const Auth: FC = () => {

  return (
    <StyledBlock>
      Auth
    </StyledBlock>
  );
};
