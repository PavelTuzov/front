/** Библиотеки */
import * as React from 'react';
import { FC, PropsWithChildren } from "react";
import { StyledBlock } from "./styles";
import { useTranslation } from "next-i18next";
import { ContentProps } from "./interfaces";

export const Content: FC<PropsWithChildren<ContentProps>> = ({ content, children }: ContentProps) => {
  const { t, i18n } = useTranslation('common');
  const lang = i18n.language;

  return (
    <StyledBlock>
      <h1>{content.title}</h1>
      <p dangerouslySetInnerHTML={{__html: content.body}} ></p>
    </StyledBlock>
  );
};
