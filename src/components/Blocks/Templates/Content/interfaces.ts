import { ReactNode } from "react";
import { ContentInfo } from "~/components/Page/types";

export type ContentProps = {
  children?: ReactNode
  content?: ContentInfo
}