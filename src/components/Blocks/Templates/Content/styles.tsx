import styled from "@emotion/styled";

export const StyledBlock = styled.div`
  background: #fff;
  padding: 2rem;
  border: 1px solid #eee;
  border-radius: 4px;
`;