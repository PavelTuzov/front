import {HtmlIcon, CssIcon, ReactIcon, VueIcon, MongoIcon, WebPackIcon, NodeIcon, NextIcon, PhpIcon, LaravelIcon, SymfonyIcon, GoIcon, MysqlIcon, DockerIcon} from "~/components/icons/techs/";


export const DURATION = 15000;
export const ROWS = 8;


export const MAP_ICONS = {
  docker:<DockerIcon/>, mysql: <MysqlIcon/>, go: <GoIcon/>, symfony: <SymfonyIcon/>, laravel: <LaravelIcon/>, php: <PhpIcon/>, next:<NextIcon/>, node:<NodeIcon/>, webpack: <WebPackIcon/>, mongo: <MongoIcon/>, vue:<VueIcon/>, react:<ReactIcon/>, css:<CssIcon/>, html:<HtmlIcon/>
}
/*

export const TAGS = [
  {
    id: 1,
    title: 'HTML',
    secondColor: '#E1471B',
    color: '#fff',
    logo: <HtmlIcon/>,
    body: {
      ru: "HTML (от англ. HyperText Markup Language — «язык гипертекстовой разметки») — стандартизированный язык разметки документов во Всемирной паутине. Большинство веб-страниц содержат описание разметки на языке HTML (или XHTML). Язык HTML интерпретируется браузерами; полученный в результате интерпретации форматированный текст отображается на экране монитора компьютера или мобильного устройства.",
      en: "HTML (HyperText Markup Language) is the code that is used to structure a web page and its content. For example, content could be structured within a set of paragraphs, a list of bulleted points, or using images and data tables. As the title suggests, this article will give you a basic understanding of HTML and its functions."
    }
  },
  {
    id: 2,
    title: 'CSS',
    secondColor: '#214CE4',
    color: '#fff',
    logo: <CssIcon/>,
    body: {
      ru: "Аббревиатура CSS расшифровывается как Cascading Style Sheets, что в переводе означает «каскадные таблицы стилей». Это язык разметки, используемый для визуального оформления веб-сайтов. ",
      en: "CSS (Cascading Style Sheets) allows you to create great-looking web pages, but how does it work under the hood? This article explains what CSS is with a simple syntax example and also covers some key terms about the language.",
    }
  },
  {
    id: 3,
    title: 'React',
    secondColor: '#00D8FE',
    color: '#fff',
    logo: <ReactIcon/>,
    body: {
      ru: "HTML (от англ. HyperText Markup Language — «язык гипертекстовой разметки») — стандартизированный язык разметки документов во Всемирной паутине. Большинство веб-страниц содержат описание разметки на языке HTML (или XHTML). Язык HTML интерпретируется браузерами; полученный в результате интерпретации форматированный текст отображается на экране монитора компьютера или мобильного устройства.",
      en: "HTML (HyperText Markup Language) is the most basic building block of the Web. It defines the meaning and structure of web content. "
    }
  },
  {
    id: 4,
    title: 'VueJs',
    secondColor: '#3FB883',
    color: '#fff',
    logo: <VueIcon/>,
    body: {
      ru: "HTML (от англ. HyperText Markup Language — «язык гипертекстовой разметки») — стандартизированный язык разметки документов во Всемирной паутине. Большинство веб-страниц содержат описание разметки на языке HTML (или XHTML). Язык HTML интерпретируется браузерами; полученный в результате интерпретации форматированный текст отображается на экране монитора компьютера или мобильного устройства.",
      en: "HTML (HyperText Markup Language) is the most basic building block of the Web. It defines the meaning and structure of web content. "
    }
  },
  {
    id: 5,
    title: 'MongoDB',
    secondColor: '#599536',
    color: '#fff',
    logo: <MongoIcon/>,
    body: {
      ru: "HTML (от англ. HyperText Markup Language — «язык гипертекстовой разметки») — стандартизированный язык разметки документов во Всемирной паутине. Большинство веб-страниц содержат описание разметки на языке HTML (или XHTML). Язык HTML интерпретируется браузерами; полученный в результате интерпретации форматированный текст отображается на экране монитора компьютера или мобильного устройства.",
      en: "HTML (HyperText Markup Language) is the most basic building block of the Web. It defines the meaning and structure of web content. "
    }
  },
  {
    id: 6,
    title: 'WebPack',
    secondColor: '#1B78BF',
    color: '#fff',
    logo: <WebPackIcon/>,
    body: {
      ru: "HTML (от англ. HyperText Markup Language — «язык гипертекстовой разметки») — стандартизированный язык разметки документов во Всемирной паутине. Большинство веб-страниц содержат описание разметки на языке HTML (или XHTML). Язык HTML интерпретируется браузерами; полученный в результате интерпретации форматированный текст отображается на экране монитора компьютера или мобильного устройства.",
      en: "HTML (HyperText Markup Language) is the most basic building block of the Web. It defines the meaning and structure of web content. "
    }
  },
  {
    id: 7,
    title: 'NodeJs',
    secondColor: '#8CC74A',
    color: '#fff',
    logo: <NodeIcon/>,
    body: {
      ru: "HTML (от англ. HyperText Markup Language — «язык гипертекстовой разметки») — стандартизированный язык разметки документов во Всемирной паутине. Большинство веб-страниц содержат описание разметки на языке HTML (или XHTML). Язык HTML интерпретируется браузерами; полученный в результате интерпретации форматированный текст отображается на экране монитора компьютера или мобильного устройства.",
      en: "HTML (HyperText Markup Language) is the most basic building block of the Web. It defines the meaning and structure of web content. "
    }
  },
  {
    id: 8,
    title: 'NextJs',
    secondColor: '#333',
    color: '#fff',
    logo: <NextIcon/>,
    body: {
      ru: "HTML (от англ. HyperText Markup Language — «язык гипертекстовой разметки») — стандартизированный язык разметки документов во Всемирной паутине. Большинство веб-страниц содержат описание разметки на языке HTML (или XHTML). Язык HTML интерпретируется браузерами; полученный в результате интерпретации форматированный текст отображается на экране монитора компьютера или мобильного устройства.",
      en: "HTML (HyperText Markup Language) is the most basic building block of the Web. It defines the meaning and structure of web content. "
    }
  },
  {
    id: 9,
    title: 'PHP',
    secondColor: '#777BB3',
    color: '#fff',
    logo: <PhpIcon/>,
    body: {
      ru: "HTML (от англ. HyperText Markup Language — «язык гипертекстовой разметки») — стандартизированный язык разметки документов во Всемирной паутине. Большинство веб-страниц содержат описание разметки на языке HTML (или XHTML). Язык HTML интерпретируется браузерами; полученный в результате интерпретации форматированный текст отображается на экране монитора компьютера или мобильного устройства.",
      en: "HTML (HyperText Markup Language) is the most basic building block of the Web. It defines the meaning and structure of web content. "
    }
  },
  {
    id: 10,
    title: 'Laravel',
    secondColor: '#FF2C1F',
    color: '#fff',
    logo: <LaravelIcon/>,
    body: {
      ru: "HTML (от англ. HyperText Markup Language — «язык гипертекстовой разметки») — стандартизированный язык разметки документов во Всемирной паутине. Большинство веб-страниц содержат описание разметки на языке HTML (или XHTML). Язык HTML интерпретируется браузерами; полученный в результате интерпретации форматированный текст отображается на экране монитора компьютера или мобильного устройства.",
      en: "HTML (HyperText Markup Language) is the most basic building block of the Web. It defines the meaning and structure of web content. "
    }
  },
  {
    id: 11,
    title: 'Symfony',
    secondColor: '#000',
    color: '#fff',
    logo: <SymfonyIcon/>,
    body: {
      ru: "HTML (от англ. HyperText Markup Language — «язык гипертекстовой разметки») — стандартизированный язык разметки документов во Всемирной паутине. Большинство веб-страниц содержат описание разметки на языке HTML (или XHTML). Язык HTML интерпретируется браузерами; полученный в результате интерпретации форматированный текст отображается на экране монитора компьютера или мобильного устройства.",
      en: "HTML (HyperText Markup Language) is the most basic building block of the Web. It defines the meaning and structure of web content. "
    }
  },
  {
    id: 11,
    title: 'Go',
    secondColor: '#000',
    color: '#fff',
    logo: <GoIcon/>,
    body: {
      ru: "HTML (от англ. HyperText Markup Language — «язык гипертекстовой разметки») — стандартизированный язык разметки документов во Всемирной паутине. Большинство веб-страниц содержат описание разметки на языке HTML (или XHTML). Язык HTML интерпретируется браузерами; полученный в результате интерпретации форматированный текст отображается на экране монитора компьютера или мобильного устройства.",
      en: "HTML (HyperText Markup Language) is the most basic building block of the Web. It defines the meaning and structure of web content. "
    }
  },
  {
    id: 12,
    title: 'Mysql',
    secondColor: '#01546B',
    color: '#fff',
    logo: <MysqlIcon/>,
    body: {
      ru: "HTML (от англ. HyperText Markup Language — «язык гипертекстовой разметки») — стандартизированный язык разметки документов во Всемирной паутине. Большинство веб-страниц содержат описание разметки на языке HTML (или XHTML). Язык HTML интерпретируется браузерами; полученный в результате интерпретации форматированный текст отображается на экране монитора компьютера или мобильного устройства.",
      en: "HTML (HyperText Markup Language) is the most basic building block of the Web. It defines the meaning and structure of web content. "
    }
  },
  {
    id: 13,
    title: 'Docker',
    secondColor: '#066da5',
    color: '#fff',
    logo: <DockerIcon/>,
    body: {
      ru: "HTML (от англ. HyperText Markup Language — «язык гипертекстовой разметки») — стандартизированный язык разметки документов во Всемирной паутине. Большинство веб-страниц содержат описание разметки на языке HTML (или XHTML). Язык HTML интерпретируется браузерами; полученный в результате интерпретации форматированный текст отображается на экране монитора компьютера или мобильного устройства.",
      en: "HTML (HyperText Markup Language) is the most basic building block of the Web. It defines the meaning and structure of web content. "
    }
  },

]*/
