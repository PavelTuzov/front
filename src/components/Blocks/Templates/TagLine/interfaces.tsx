import {ReactNode} from "react";

export interface InfiniteLoopSliderProps {
  children: ReactNode,
  duration: number,
  reverse: boolean,
  selectedTag: number,
}


export interface TagLineProps {
  params?: TagLineItemProp[];
  height?: string;
}


export type TagLineItemProp = {
  title?: string
  body?: string
  image?: string
  action?: string

  title_en?: string
  body_en?: string
  image_en?: string
  action_en?: string

  color: string
  url?: string
}
