import styled from "@emotion/styled";

export const StyledLoopSliderInner = styled.div<{ duration, direction }>`
  ${({ duration, direction }) => {
    return `
        display: flex;
        width: fit-content;
        animation-name: loop;
        animation-timing-function: linear;
        animation-iteration-count: infinite;
        animation-direction: ${direction};
        animation-duration: ${duration}ms;
    `;
  }}
`;

export const StyledLoopSlider = styled.div `
  
`

export const StyledTech = styled.div`
  display: flex;
  .tech-logo {
    flex: 2;
    margin: 0 1rem 0 0;
    & svg {
      max-width: 96px;
      max-height: 96px;
    }
  }
  &-body {
    flex: 6;
  }
`;

export const StyledBlock = styled.div`
  background: #fff;
  min-height: 100px;
  border-radius: 4px;
  overflow: hidden;
  max-height: 300px;
  position: relative;

  & .tag-list-overlay {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background: rgba(255, 255, 255, 0.8);
    content: '';
    transition: opacity 0.5s ease-out;
    opacity: 1;
    padding: 2rem;
    overflow: hidden;
    display: flex;
    flex-direction: column;
    justify-content: center;

    & span {
      display: block;
      &:first-of-type {
        margin: 0 0 1rem 0;
      }
    }
  }

  &:hover {
    & .tag-list-overlay{
      opacity: 0;
      padding: 0;
      height: 0;
    }
  }

  & .tag-list {
    width: 120%;
    margin: -10% 0 0 -10%;
    display: flex;
    flex-shrink: 0;
    flex-direction: column;
    gap: 1rem 0;
    position: relative;
    padding: 1.5rem 0;
    overflow: hidden;
    transform: rotate(15deg);
  }

  @keyframes loop {
    0% {
      transform: translateX(0);
    }
    100% {
      transform: translateX(-50%);
    }
  }
`;


