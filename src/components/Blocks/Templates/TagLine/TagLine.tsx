/** Библиотеки */
import * as React from 'react';
import {FC, useEffect, useRef, useState} from "react";
import { StyledBlock, StyledLoopSlider, StyledLoopSliderInner, StyledTech } from "./styles";
import { StyledGradientText } from "../../../common/GradientText/style";
import { useTranslation } from "next-i18next";
import { DURATION, ROWS } from "~/components/Blocks/Templates/TagLine/constants";
import { InfiniteLoopSliderProps, TagLineProps } from "~/components/Blocks/Templates/TagLine/interfaces";
import { SidePage, UsedLocales } from 'front.ui.lib';
import { TagProps } from "~/components/TagTech/interfaces";
import TagTech from "~/components/TagTech/TagTech";
import SimpleForm from "~/components/Forms/SimpleForm/SimpleForm";
import { nanoid } from "nanoid";
import Portal from "~/components/Portal/Portal";
import { getTechList, getTech } from "~/api/api";
import { MAP_ICONS } from "~/components/Blocks/Templates/TagLine/constants";


export const TagLine: FC<TagLineProps> = ({ params, height }: TagLineProps) => {
  const { t, i18n } = useTranslation('common');
  const lang = i18n.language;
  const [isPageOpen, setIsPageOpen] = useState<boolean>(false);
  const [tagData, setTagData] = useState<TagProps>(null);

  const [tagsLoad, setTagsLoad] = useState<boolean>(false);
  const [tagLoad, setTagLoad] = useState<boolean>(false);
  const [tags, setTags] = useState<TagProps[]>([]);
  const [selectedTag, setSelectedTag] = useState<number>(undefined);

  useEffect(() => {
    if (!isPageOpen) {
      setTagLoad(false);
      setSelectedTag(undefined)
    }
  }, [isPageOpen])

  useEffect(() => {
    setTagLoad(false);

    const fetchTagsData = async () => {
      const result = await getTechList();
      setTags(result);
      setTagsLoad(true);
    };

    fetchTagsData()
      .catch((e) => {
        console.log(e.message);
      });
  }, []);

  useEffect(() => {
    setTagLoad(true);
    const fetchTagData = async () => {
      const result = await getTech(selectedTag);
      setTagData(result);
      setTagLoad(false);
    };

    if (selectedTag) {
      fetchTagData()
        .catch((e) => {
          console.log(e.message);
        });
      setIsPageOpen(true);
    }
  }, [selectedTag])

  const random = (min, max) => Math.floor(Math.random() * (max - min)) + min;
  const shuffle = (arr) => [...arr].sort( () => .5 - Math.random());

  const handleTagClick = (id: number): void => {
    setSelectedTag(id);
  }

  return (
    <StyledBlock>
      {tagsLoad && (
        <>
          <div className='tag-list'>
            {[...new Array(ROWS)].map((_, i) => (
              <InfiniteLoopSlider
                selectedTag={selectedTag}
                key={i}
                duration={random(DURATION - 5000, DURATION + 5000)}
                reverse={i % 2 !== 0}
              >
                {shuffle(tags).map((tag: TagProps) => (
                  <TagTech
                    {...tag}
                    key={nanoid(5)}
                    onClick={() => handleTagClick(tag.id)}
                  />
                ))}
              </InfiniteLoopSlider>
            ))}
          </div>
          {!selectedTag && (
            <div className='tag-list-overlay'>
              {params[0]?.title && params[0]?.title_en && (
                <StyledGradientText size={2.4} lineHeight={3} bold={true}>
                  {lang === UsedLocales.ru ? params[0].title : params[0].title_en}
                </StyledGradientText>
              )}
              {params[0]?.body && params[0]?.body_en && (<StyledGradientText size={1.4} lineHeight={2}>
                {lang === UsedLocales.ru ? params[0].body : params[0].body_en}
              </StyledGradientText>)}
            </div>
          )}
          {tagData && (
            <Portal>
              <SidePage
                show={isPageOpen}
                onCloseClick={() => setIsPageOpen(false)}
                headerText={tagData.title}
              >
                <StyledTech>
                  <div className={'tech-logo'}>{MAP_ICONS[tagData.logo]}</div>
                  <div className={'tech-body'} dangerouslySetInnerHTML={{__html: lang === UsedLocales.ru ? tagData.body : tagData.body_en}}>
                  </div>
                </StyledTech>

                <SimpleForm/>
              </SidePage>
            </Portal>
          )}
        </>
      )}
    </StyledBlock>
  );
};


const InfiniteLoopSlider: FC<InfiniteLoopSliderProps> = ({
     children,
     duration,
     selectedTag,
     reverse = false
   }: InfiniteLoopSliderProps) => {
  const loopRef = useRef() as React.MutableRefObject<HTMLDivElement>;

  const mouseOverHandler = () => {
    loopRef.current.style.animationPlayState = 'paused'
  }

  const mouseLeaveHandler = () => {
    loopRef.current.style.animationPlayState = 'running'
  }

  useEffect(() => {
    if (!selectedTag) {
      loopRef.current.style.animationPlayState = 'running'
    }
  }, [selectedTag])


  return (
    <StyledLoopSlider
      onMouseEnter={() => mouseOverHandler()}
      onMouseLeave={() => mouseLeaveHandler()}
    >
      <StyledLoopSliderInner
        duration={duration}
        direction={reverse ? 'reverse' : 'normal'}
        ref={loopRef}
      >
        {children}
        {children}
      </StyledLoopSliderInner>
    </StyledLoopSlider>
  );
};