import { ReactNode } from "react";

export type ServiceProps = {
  height?: number
  block_id: number
  children?: ReactNode
}

export type ServiceListProps = {

}
