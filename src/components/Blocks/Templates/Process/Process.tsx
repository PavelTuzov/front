import React, {useEffect, useMemo, useState} from 'react';
import { StyledProcess, StyledProcessHeader, StyledProcessBody, StyledProcessBodyItem, StyledProcessItem, StyledProcessBodyItemHeader, StyledProcessBodyItemHeaderStep } from "./styles";
import { H3, H2, Text } from 'front.ui.lib';
import { Step, StepItem } from "~/components/Blocks/Templates/Process/interfaces";
import { useTranslation } from "next-i18next";
import type { TFunction } from 'react-i18next';
import { useRouter } from "next/dist/client/router";

const getSteps = (t: TFunction<'common'>) => {
  return [
    {
      title: t('steps.0.title'),
      body: t('steps.0.body'),
      steps: [
        {
          title: t('steps.0.steps.0.title'),
          step: 1,
          list: [
            t('steps.0.steps.0.list.0'),
            t('steps.0.steps.0.list.1'),
            t('steps.0.steps.0.list.2'),
          ]
        }
      ]
    },
    {
      title: t('steps.1.title'),
      body: t('steps.1.body'),
      steps: [
        {
          title: t('steps.1.steps.0.title'),
          step: 2,
          list: [
            t('steps.1.steps.0.list.0'),
            t('steps.1.steps.0.list.1'),
            t('steps.1.steps.0.list.2'),
            t('steps.1.steps.0.list.3'),
            t('steps.1.steps.0.list.4'),
          ]
        }
      ]
    },
    {
      title: t('steps.2.title'),
      body: t('steps.2.body'),
      steps: [
        {
          title: t('steps.2.steps.0.title'),
          step: 3,
          list: [
            t('steps.2.steps.0.list.0'),
            t('steps.2.steps.0.list.1'),
            t('steps.2.steps.0.list.2'),
            t('steps.2.steps.0.list.3'),
            t('steps.2.steps.0.list.4'),
          ]
        },
        {
          title: t('steps.2.steps.1.title'),
          step: 4,
          list: [
            t('steps.2.steps.1.list.0'),
            t('steps.2.steps.1.list.1'),
            t('steps.2.steps.1.list.2'),
            t('steps.2.steps.1.list.3'),
          ]
        },
        {
          title: t('steps.2.steps.2.title'),
          step: 5,
          list: [
            t('steps.2.steps.2.list.0'),
            t('steps.2.steps.2.list.1'),
            t('steps.2.steps.2.list.2'),
            t('steps.2.steps.2.list.3'),
          ]
        }
      ]
    },
    {
      title: t('steps.3.title'),
      body: t('steps.3.body'),
      steps: [
        {
          title: t('steps.3.steps.0.title'),
          step: 6,
          list: [
            t('steps.3.steps.0.list.0'),
            t('steps.3.steps.0.list.1'),
            t('steps.3.steps.0.list.2'),
            t('steps.3.steps.0.list.3'),
            t('steps.3.steps.0.list.4'),
            t('steps.3.steps.0.list.5'),
          ]
        }
      ]
    },
  ]
}

export const Process = () => {
  const SERVICE_KEY = 'processWebService'
  const { t } = useTranslation('common', { keyPrefix: SERVICE_KEY });
  const [steps, setSteps] = useState<Step[]>([]);

  useEffect(() => {
    setSteps(getSteps(t))
  }, [t]);

  return (
    <StyledProcess>
      <H2>{t('title')}</H2>
      {steps.length && steps.map((step, index) => {
        return (
          <StyledProcessItem key={index}>
            <StyledProcessHeader>
              <H3>{step.title}</H3>
              <Text>{step.body}</Text>
            </StyledProcessHeader>
            <StyledProcessBody>
              {step.steps.map((stepItem: StepItem, stepIndex) => {
                return (
                  <StyledProcessBodyItem key={stepIndex}>
                    <StyledProcessBodyItemHeader>
                      <StyledProcessBodyItemHeaderStep>{stepItem.step}</StyledProcessBodyItemHeaderStep>
                      <H3>{stepItem.title}</H3>
                    </StyledProcessBodyItemHeader>
                    <ul>
                      {stepItem.list.map((stepItemList, stepItemIndex) => <li key={stepItemIndex}>{stepItemList}</li>)}
                    </ul>
                  </StyledProcessBodyItem>
                )
              })}
            </StyledProcessBody>
          </StyledProcessItem>
        )
      })}
    </StyledProcess>
  );
};

export default Process;