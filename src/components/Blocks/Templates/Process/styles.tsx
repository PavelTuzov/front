import styled from "@emotion/styled";
import {defaultTheme} from "front.ui.lib";
import {desktop940} from "~/components/Grid/constants";

export const StyledProcess = styled.div`
  background: #fff;
  border-radius: 4px;
  padding: 1rem;

  & h2 {
    margin: 1rem 1rem 2rem 1rem;
  }
`

export const StyledProcessHeader = styled.div`
  background: ${defaultTheme.colors.lightGrey};
  padding: 2rem 1rem;
  flex: 2;
  min-width: 310px;
  max-width: 310px;

  & h3 {
    font-size: 1rem;
    line-height: 1.1rem;
  }

  @media (max-width: ${desktop940}px) {
    padding: 1rem;
    max-width: 100%;
    min-width: 100%;
    & h3 {
      margin: 0 0 1rem 0;
    }
  }
`;

export const StyledProcessBody = styled.div`
  padding: 2rem 2rem;
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: 2rem;

  & h3 {
    font-size: 1.2rem;
    line-height: 1.3rem;
  }

  & ul {
    list-style: none;
    padding: 0;
    margin: 0;

    & li {
      margin: 0 0 0.5rem 0;
    }
  }

  @media (max-width: ${desktop940}px) {
    padding: 1rem;
    grid-template-columns: 1fr;
    grid-gap: 1rem;
  }
`;

export const StyledProcessBodyItem = styled.div`
  margin: 0 0 2rem 0;
`;

export const StyledProcessBodyItemHeader = styled.div`
  display: flex;
  margin: 0 0 1rem 0;
  align-items: center;
  position: relative;

  & h3 {
    padding: 0;
    font-size: 2rem;
    line-height: 2.2rem;
  }

  @media (max-width: ${desktop940}px) {
    & h3 {
      font-size: 1.2rem;
      line-height: 1.4rem;
    }
  }
`;

export const StyledProcessBodyItemHeaderStep = styled.span`
  font-size: 5rem;
  display: flex;
  align-items: center;
  align-content: center;
  justify-content: center;
  font-weight: bold;
  width: 5rem;
  height: 5rem;
  position: absolute;
  left: -2rem;
  opacity: 0.2;

  @media (max-width: ${desktop940}px) {
    width: 3rem;
    min-width: 3rem;
    height: 3rem;
    font-size: 2rem;
  }
`;

export const StyledProcessItem = styled.div`
  border-bottom: 1px solid #ccc;
  display: flex;

  &:first-of-type {
    ${StyledProcessHeader} {
      border-top-left-radius: 3px;
      border-top-right-radius: 3px;
    }
  }

  &:last-of-type {
    border-bottom: none;

    ${StyledProcessHeader} {
      border-bottom-left-radius: 3px;
      border-bottom-right-radius: 3px;
    }
  }

  @media (max-width: ${desktop940}px) {
    display: block;
    width: 100%;
  }
`;