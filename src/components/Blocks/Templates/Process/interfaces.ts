export interface Step {
  title: string
  body: string
  steps: StepItem[]
}

export interface StepItem {
  title: string
  step: number
  list: string[]
}