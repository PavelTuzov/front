import styled from "@emotion/styled";
import {desktop430, desktop450, desktop940} from "~/components/Grid/constants";

export const StyledHero = styled.div`
  width: 100%;
  height: calc(100vh - 64px - 4rem);
  display: flex;
  align-items: center;
  align-content: center;
  justify-content: center;
  border-radius: 4px;
  background: #fff;
`;

export const StyledHeroItem = styled.div`
  display: block;
  width: 100%;
  height: 100%;
  position: relative;
`;


export const StyledHeroItemImage = styled.div<{img?: string}>`
  ${({ theme, img }: any) => {
    return `
      width: 100%;
      height: 100%;
      background-image: url(${img});
      background-repeat: no-repeat;
      background-position: 50% 50%;
      border-radius: 4px;
    `
  }}
`;

export const StyledHeroButton = styled.div`
  margin: 2rem 0 0 0;
`;

export const StyledHeroItemContent = styled.div`
  ${({ theme, img }: any) => {
  return `
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 2;
    background: rgba(0, 0, 0, 0.8);
    border-radius: 4px;
    color: #fff;
    display: flex;
    padding: 2rem;
    align-items: flex-start;
    align-content: center;
    flex-direction: column;
    justify-content: center;
    & h1 {
      font-size: 3.5rem;
      line-height: 4rem;
      margin: 0;
      font-weight: 700;
      padding: 0;
      max-width: 70%;
      display: inline;
    }
    @media (max-width: ${desktop940}px) {
      & h1 {
        font-size: 2rem;
        line-height: 2rem;
        max-width: 90%;
      }
    }
    
    @media (max-width: ${desktop450}px) {
      & h1 {
        font-size: 2rem;
        line-height: 2rem;
        max-width: 100%;
      }
    }
  `}};
`;

export const StyledHeroItemContentBody = styled.p`
  margin: 2rem 0 0 0;
  font-size: 1.2rem;
  line-height: 1.6rem;
  max-width: 70%;
  opacity: 0.6;

  @media (max-width: ${desktop940}px) {
    font-size: 1rem;
    line-height: 1.4rem;
    max-width: 90%;
  }
`