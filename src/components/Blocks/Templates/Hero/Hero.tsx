/** Библиотеки */
import * as React from 'react';
import {FC, useEffect, useState} from "react";
import { StyledHero, StyledHeroItem, StyledHeroItemImage, StyledHeroItemContent, StyledHeroItemContentBody, StyledHeroButton } from "./styles";
import { useTranslation } from "next-i18next";
import { HeroProps } from "./interfaces";
import { UsedLocales, Button, ButtonSizeTypes, ButtonStyleTypes, SidePage } from "front.ui.lib";
import ResultForm from "~/components/Forms/ResultForm/ResultForm";
import { FormResult } from "~/components/Forms/ResultForm/interfaces";
import Portal from "~/components/Portal/Portal";
import { ConsultForm } from "~/components/Forms/ConsultForm/ConsultForm";
import { Manager } from "~/components/common/Manager/Manager";

const DEFAULT_DEP = 1;
const WIDTH_DEFAULT_SIDEPAGE = 90;
const WIDTH_RESULT_SIDEPAGE = 60;


export const Hero: FC<HeroProps> = ({ params }) => {
  const { t, i18n } = useTranslation('common');
  const lang = i18n.language;
  const [isFormOpen, setIsFormOpen] = useState<boolean>(false);
  const [isFormFinished, setIsFormFinished] = useState<boolean>(false);
  const [selectedDepartment, setSelectedDepartment] = useState<number>(DEFAULT_DEP)
  const [showManagerCol, setShowManagerCol] = useState<boolean>(true);
  const [widthConsultForm, setWidthConsultForm] = useState<number>(WIDTH_DEFAULT_SIDEPAGE);

  const handleRecallFormFinish = () => {
    setIsFormFinished(true);
    setShowManagerCol(false);
    setWidthConsultForm(WIDTH_RESULT_SIDEPAGE);
  }

  useEffect(() => {
    setTimeout(() => {
      setIsFormFinished(false);
      setWidthConsultForm(WIDTH_DEFAULT_SIDEPAGE);
    }, 400);

    setShowManagerCol(true);
    setSelectedDepartment(DEFAULT_DEP);
  }, [isFormOpen])

  useEffect(() => {
    setWidthConsultForm(isFormOpen ? showManagerCol ? WIDTH_DEFAULT_SIDEPAGE : WIDTH_RESULT_SIDEPAGE : WIDTH_RESULT_SIDEPAGE);
  }, [showManagerCol, isFormOpen])

  const slides = params ? params.map((item, index) => {
    return (
      <StyledHeroItem key={index}>
        <StyledHeroItemContent>
          <h1>
            <span>{lang === UsedLocales.ru ? item.title : item.title_en}</span>
          </h1>
          <StyledHeroItemContentBody>
            {lang === UsedLocales.ru ? item.body : item.body_en}
          </StyledHeroItemContentBody>
          <StyledHeroButton>
            <Button
              onClick={() => setIsFormOpen(!isFormOpen)}
              styleSize={ButtonSizeTypes.LARGE}
              styleType={ButtonStyleTypes.PRIMARY}
              tabIndex={0}
            >
              {t('consult')}
            </Button>
          </StyledHeroButton>
        </StyledHeroItemContent>
        <StyledHeroItemImage img={lang === UsedLocales.ru ? item.image : item.image_en} />

        <Portal>
          <SidePage
            width={`${widthConsultForm}%`}
            show={isFormOpen}
            onCloseClick={() => setIsFormOpen(false)}
            headerText={isFormFinished ? null : t('recallForm.title')}
            sideBar={<Manager selectedDepartment={selectedDepartment} setShowManagerCol={setShowManagerCol}/>}
            showSideBar={showManagerCol}
          >
            {isFormFinished ? (
              <ResultForm title={t('form.success')} result={FormResult.SUCCESS} />
            ) : (
              <ConsultForm
                onFinish={handleRecallFormFinish}
                selectDepartment={(id) => setSelectedDepartment(id)}
                selectedDepartment={selectedDepartment}
              />
            )}
          </SidePage>
        </Portal>
      </StyledHeroItem>
    )
  }) : (<></>);

  return (
    <StyledHero>
      {slides}
    </StyledHero>
  );
};
