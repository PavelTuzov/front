import {ReactNode} from "react";
import { SliderItemProp } from "~/components/Blocks/Templates/Banner/BannerProps";

export type HeroProps = {
  params: SliderItemProp[]
  height?: number
  block_id: number
  children?: ReactNode
}
