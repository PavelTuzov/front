/** Библиотеки */
import * as React from 'react';
import { FC } from "react";
import {
  StyledBlock,
  StyledBlockItem,
  StyledBlockItemBody,
  StyledBlockItemTitle,
  StyledBlockItemFooter,
  StyledBlockItemText,
  StyledBlockItemImage
} from "~/components/Blocks/Templates/Banner/styles";
import { useTranslation } from "next-i18next";
import { BannerProps } from "~/components/Blocks/Templates/Banner/BannerProps";
import { BaseSlider, UsedLocales, ButtonSizeTypes, ButtonStyleTypes, Button } from 'front.ui.lib';
import Link from "next/dist/client/link";

export const Banner: FC<BannerProps> = ({ params, height }: BannerProps) => {
  const { t, i18n } = useTranslation('common');
  const lang = i18n.language;

  const slides = params ? params.map(item => {
    return (
      <>
        <Link href={item.url}>
          <StyledBlockItem>
            <StyledBlockItemText bgColor={item.bgColor} color={item.color}>
              <StyledBlockItemTitle>{lang === UsedLocales.ru ? item.title : item.title_en}</StyledBlockItemTitle>
              <StyledBlockItemBody>{lang === UsedLocales.ru ? item.body : item.body_en}</StyledBlockItemBody>
              {item.action && (
                <StyledBlockItemFooter>

                      <Button
                        styleSize={ButtonSizeTypes.DEFAULT}
                        styleType={ButtonStyleTypes.SECONDARY}
                        tabIndex={0}>
                        {lang === UsedLocales.ru ? item.action : item.action_en}
                      </Button>

                </StyledBlockItemFooter>
              )}
            </StyledBlockItemText>
            <StyledBlockItemImage img={lang === UsedLocales.ru ? item.image : item.image_en}/>
          </StyledBlockItem>
        </Link>
      </>
    )
  }) : [(<></>)];

  return (
    <StyledBlock>
      <BaseSlider
        slides={slides}
        height={height}
        lang={i18n.language as UsedLocales}
        autoPlay={2500}
      />
    </StyledBlock>
  );
};
