import styled from "@emotion/styled";
import {desktop940} from "~/components/Grid/constants";

export const StyledBlock = styled.div`
  background: #ccc;
  min-height: 100px;
  border-radius: 4px;
  overflow: hidden;
  & .swiper {
    &-slide {
      min-height: 200px;
    }
  }
`;

export const StyledBlockItemText = styled.div<{ bgColor: string, color: string }>`
  ${({ theme, bgColor, color }: any) => {
    return `
      background-color: ${bgColor};
      padding: 3rem;
      height: 100%;
      width: 40%;
      position: relative;
      position: relative;
      z-index: 3;
      color: ${color};
      & button {
        border-color: ${color};
        & .button__content {
          color: ${color};
        }
      }
      
      &::after {
        z-index: -1;
        content: '';
        position: absolute;
        width: 400px;
        height: 400px;
        transform: skew(-15deg);
        bottom: 0;
        right: -60px;
        background-color: ${bgColor};
        transition: all 0.3s;
      }
      
      @media (max-width: ${desktop940}px) {
        padding: 1rem;
      }
    `;
  }}
`;

export const StyledBlockItemImage = styled.div<{img?: string}>`
${({ theme, img }: any) => {
  return `
    width: 60%;
    background-image: url(${img});
    background-repeat: no-repeat;
    background-position: 50% 50%;
  `
}}
`;
export const StyledBlockItem = styled.a<{ img?: string }>`
  ${({ theme, img }: any) => {
    return `
      cursor: pointer;
      display: flex;
      width: 100%;
      height: 100%;
      text-align: left;
      overflow: hidden;
      &:hover {
        ${StyledBlockItemText} {
          &::after {
            transform: skew(-15deg) scaleX(1.1);
          }
        }
      }
    `
  }}
`;

export const StyledBlockItemTitle = styled.p`
  font-size: 2rem;
  font-weight: bold;
  line-height: 2.4rem;
  margin: 0 0 1rem 0;
  padding: 0;

  @media (max-width: ${desktop940}px) {
    font-size: 1.4rem;
    line-height: 2rem;
    margin: 0 0 0.5rem 0;
  }
`;

export const StyledBlockItemBody = styled.p`
  font-size: 1rem;
  line-height: 1.4rem;
  margin: 0;
  padding: 0;

  @media (max-width: ${desktop940}px) {
    font-size: 0.7rem;
    line-height: 1rem;
  }
`;

export const StyledBlockItemFooter = styled.div`
    margin: 1rem 0 0 0;
`;