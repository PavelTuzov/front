import { ReactNode } from "react";

export type BannerProps = {
  params: SliderItemProp[]
  height?: number
  block_id: number
  children?: ReactNode
}

export type SliderItemProp = {
  title?: string
  body?: string
  image?: string
  action?: string

  title_en?: string
  body_en?: string
  image_en?: string
  action_en?: string

  color: string
  bgColor: string
  url?: string
}
