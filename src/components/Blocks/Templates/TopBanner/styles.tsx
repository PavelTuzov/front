import styled from "@emotion/styled";

export const StyledBlock = styled.div`
  background: #ccc;
  min-height: 200px;
  border-radius: 4px;
  overflow: hidden;
  & .swiper {
    &-slide {
      min-height: 200px;
    }
  }
`;