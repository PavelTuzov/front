/** Библиотеки */
import * as React from 'react';
import { FC, PropsWithChildren } from "react";
import { StyledBlock } from "~/components/Blocks/Templates/Banner/styles";
import { useTranslation } from "next-i18next";
import { TopBannerProps } from "./TopBannerProps";
import { BaseSlider, UsedLocales, Button, ButtonSizeTypes, ButtonStyleTypes } from 'front.ui.lib';


export const TopBanner: FC<PropsWithChildren<TopBannerProps>> = ({ params, height, children }: TopBannerProps) => {
  const { t, i18n } = useTranslation('common');
  const lang = i18n.language;

  const slides = params.map(item => {
    return (
      <>
        <h2>{lang === UsedLocales.ru ? item.title : item.title_en}</h2>
        <p>{lang === UsedLocales.ru ? item.body : item.body_en}</p>
        {item.action && (
          <Button styleSize={ButtonSizeTypes.DEFAULT} styleType={ButtonStyleTypes.SECONDARY} tabIndex={0}>{lang === UsedLocales.ru ? item.action : item.action_en}</Button>
        )}
      </>
    )
  })

  return (
    <StyledBlock>
      <BaseSlider
        height={height}
        lang={i18n.language as UsedLocales}
        slides={slides}
      />
    </StyledBlock>
  );
};
