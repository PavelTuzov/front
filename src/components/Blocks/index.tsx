import { Auth } from "~/components/Blocks/Templates/Auth/Auth";
import { Banner } from "~/components/Blocks/Templates/Banner/Banner";
import { Feedback } from "~/components/Blocks/Templates/Feedback/Feedback";
import { Showcase } from "~/components/Blocks/Templates/Showcase/Showcase";
import { TopBanner } from "~/components/Blocks/Templates/TopBanner/TopBanner";
import { Content } from "~/components/Blocks/Templates/Content/Content";
import { TagLine } from "~/components/Blocks/Templates/TagLine/TagLine";
import { Hero } from "~/components/Blocks/Templates/Hero/Hero";
import { Process } from "~/components/Blocks/Templates/Process/Process";
import { Service, ServiceList } from "~/components/Blocks/Templates/Service";

export {
  Auth,
  Banner,
  TopBanner,
  Feedback,
  Showcase,
  Content,
  TagLine,
  Hero,
  Process,
  ServiceList,
  Service
}