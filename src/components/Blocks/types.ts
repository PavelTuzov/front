/** интерфейс селектора блоков */
import { Block } from "~/interfaces/Block.interface";
import { ContentInfo } from "~/components/Page/types";

export interface BlockSelectorProps {
  block: Block;
  content: ContentInfo
}
