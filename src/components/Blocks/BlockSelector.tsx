/** библиотеки */
import { FC, createElement } from 'react';

/** компоненты */
import {
  Auth,
  Banner,
  Feedback,
  Showcase,
  TopBanner,
  Content,
  TagLine,
  Hero,
  Process,
  ServiceList,
  Service
} from './index';

/** типы */
import { BlockSelectorProps } from '~/components/Blocks/types';

/** список компонентов */
const Components = {
  auth: Auth,
  banner: Banner,
  feedback: Feedback,
  showcase: Showcase,
  topBanner: TopBanner,
  content: Content,
  tagLine: TagLine,
  hero: Hero,
  process: Process,
  service: Service,
  serviceList: ServiceList,
};

const BlockSelector: FC<BlockSelectorProps> = ({
  block,
  content
}: BlockSelectorProps) => {


  if (Components[block.title] !== undefined) {
    return createElement(Components[block.title], { ...block, content: content });
  }
  return createElement(() => <div>Not founded block {block.title}</div>);
};

export default BlockSelector;
