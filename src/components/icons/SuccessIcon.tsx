import { FC } from 'react';

import { IconProps } from './types';

export const SuccessIcon: FC<IconProps> = ({ width = 22, height = 22 }) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 22 22">
      <path d="M11,22A11,11,0,1,0,0,11,11,11,0,0,0,11,22ZM5.707,11.293,8,13.586l8.293-8.293,1.414,1.414L8,16.414,4.293,12.707Z"/>
    </svg>
  );
};