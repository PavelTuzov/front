/** библиотеки */
import { useState, useContext, FC } from 'react';
import * as React from 'react';
import { useMediaQuery } from 'react-responsive';

/** Компоненты библиотеки */
import { Text, Input } from 'front.ui.lib';

/** api */

/** утилиты */

/** Контексты */
import { AuthContext } from '../../Providers/AuthProvider';

/** Компоненты */
import { AuthWizardContainer } from './style';



/**
 * Форма авторизации.
 */
const AuthWizard: FC = () => {
  const {
    login,
    password,
    validatePassword,
    validateLogin,
    errorPassword,
    errorLogin,
    setLogin,
    setPassword,
  } = useContext(AuthContext);

  const [authInProgress] = useState<boolean>(false);

  return (
    <>
      <AuthWizardContainer>
        <form className="wizard">
          <div className="wizard__wrapper">
            <div className="wizard__input-item">
              <div className="wizard__label">
                <label htmlFor="authWizardLogin">
                  <Text>Логин</Text>
                </label>

              </div>
              <div>
                <Input
                  id="authWizardLogin"
                  className="wizard__input wizard__input-login"
                  type="text"
                  value={login}
                  onChange={(e: React.ChangeEvent<HTMLInputElement>): void => {
                    setLogin(e.target.value);
                  }}
                  onBlur={validateLogin}
                  error={errorLogin}
                />
              </div>
            </div>

            <div className="wizard__input-item">
              <label htmlFor="authWizardPassword" className="wizard__label">
                <Text>Пароль</Text>
              </label>
              <div>
                <Input
                  id="authWizardPassword"
                  className="wizard__input wizard__input-password"
                  type="password"
                  value={password}
                  onBlur={validatePassword}
                  onChange={(e: React.ChangeEvent<HTMLInputElement>): void => {
                    setPassword(e.target.value);
                  }}
                  error={errorPassword}
                />
              </div>
            </div>


            {authInProgress && (
              //<Loader small className="wizard__status-container" />
              console.log('loading')
            )}
          </div>
        </form>
      </AuthWizardContainer>
    </>
  );
};

export default AuthWizard;
