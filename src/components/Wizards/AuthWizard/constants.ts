/** ошибка, которая отображается в крайнем случае */
export const DEFAULT_ERROR = 'DefaultException';

/** сообщения об ошибках ввода */
export enum ERROR_MESSAGE {
  AuthCountException = 'Неправильный номер договора и/или пароль',
  DefaultException = 'Что-то пошло не так',
}
