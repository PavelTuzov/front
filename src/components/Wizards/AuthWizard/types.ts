import { Dispatch, SetStateAction } from 'react';
import { Auth } from '../../../interfaces/Auth.interface';
/** форма авторизации */
export interface AuthWizardContextProps {
  login: string;
  password: string;
  setLogin: Dispatch<SetStateAction<string>>;
  setPassword: Dispatch<SetStateAction<string>>;
  serverError: string;
  setServerError: Dispatch<SetStateAction<string>>;
  setErrorLogin: Dispatch<SetStateAction<boolean>>;
  setErrorPassword: Dispatch<SetStateAction<boolean>>;
  errorPassword: boolean;
  errorLogin: boolean;
  isLoading: boolean;
  isCompleted: boolean;
  makeAuth: () => Promise<void>;
  toggleVisible: () => void;
  isVisible: boolean;
  onCloseClickSidePageAuth: () => void;
  validatePassword: () => void;
  validateLogin: () => void;
  setMakeAuthData: Dispatch<SetStateAction<Auth>>;
  makeAuthData: Auth;
}
