/** библиотеки */
import * as React from 'react';

/** типы */
import {
  AuthWizardContextProps,
} from './types';
import { useContext, useState } from 'react';
import { authByPassword } from '../../../api/api';
import {
  DEFAULT_ERROR,
  ERROR_MESSAGE,
} from './constants';
import { Auth } from '../../../interfaces/Auth.interface';

export const useMakeAuth = (): AuthWizardContextProps => {
  const [serverError, setServerError] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [login, setLogin] = useState<string>('');
  const [errorLogin, setErrorLogin] = useState<boolean>(false);
  const [errorPassword, setErrorPassword] = useState<boolean>(false);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [isCompleted, setIsCompleted] = useState<boolean>(false);
  const [isVisible, setIsVisible] = useState<boolean>(false);
  const [makeAuthData, setMakeAuthData] = useState<Auth | null>(null);


  /** Открывает/закрывает сайд пейдж */
  const toggleVisible = (): void => {
    setIsVisible(!isVisible);
  };

  /** Обработка закрытия виджета в SidePage */
  const onCloseClickSidePageAuth = (): void => {
    setIsVisible(false);
  };

  const hasLoginError = login.length < 4 || login.length > 10;
  const hasPasswordError = login.length < 6;

  const validateLogin = (): void => {
    setErrorLogin(hasLoginError);
  };

  const validatePassword = (): void => {
    setErrorPassword(hasPasswordError);
  };

  const makeAuth = async (): Promise<void> => {
    if (errorLogin) setErrorLogin(null);
    if (errorPassword) setErrorPassword(null);
    if (serverError) setServerError(null);
    setIsLoading(true);
    try {
      const res = await authByPassword(login, password);
      if (res && res.id) {
        setMakeAuthData(res);
        setIsCompleted(true);
        setIsVisible(false);
      }
    } catch (errorData) {
      if (errorData.errorMessage) {
        const err = JSON.parse(errorData.errorMessage);
        setServerError(
          err.Type in ERROR_MESSAGE
            ? ERROR_MESSAGE[err.Type]
            : ERROR_MESSAGE[DEFAULT_ERROR],
        );
      }
    }

    setIsLoading(false);
  };

  return {
    errorLogin,
    errorPassword,
    toggleVisible,
    isVisible,
    onCloseClickSidePageAuth,
    login,
    password,
    serverError,
    setServerError,
    setPassword,
    setLogin,
    setErrorLogin,
    setErrorPassword,
    isLoading,
    isCompleted,
    makeAuth,
    makeAuthData,
    setMakeAuthData,
    validatePassword,
    validateLogin,
  };
};

export default useMakeAuth;
