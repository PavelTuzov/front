/** библиотеки */
import { appWithTranslation, i18n } from 'next-i18next';
import { useRouter } from "next/dist/client/router";
import { FC, ReactElement, ReactNode } from "react";
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import Error from './_error'

/** api */

/** провайдеры */
import Providers from '../components/Providers';

/** константы */
import { DEFAULT_CITY } from '~/constants/common';

import Script from 'next/script';
import { AppProps } from "next/dist/pages/_app";
import { NextPage } from "next";
import { MainLayout } from "~/components/Layout/MainLayout";

export type NextPageWithLayout = NextPage & {
  getLayout?: (page: ReactElement) => ReactNode;
  /** Если true то пользователь должен быть авторизован */
  authRequired?: boolean;
};

type AppPropsWithLayout = AppProps & {
  Component: NextPageWithLayout;
};

const getDefaultLayout: NonNullable<NextPageWithLayout['getLayout']> = (page) => <MainLayout>{page}</MainLayout>;

export const App   = ({ Component, pageProps }) => {
  const { locale } = useRouter();
  if (!pageProps?.providerData) return null;
  const getLayout = Component.getLayout ?? getDefaultLayout;

  //console.log(pageProps);
  return (
    <>
      <Providers
        providerData={pageProps?.providerData}
      >
        {getLayout(<Component {...pageProps} />)}
      </Providers>
    </>
  );
};

App.getInitialProps = async (appContext) => {
  const component = {...appContext};
  const request = appContext?.ctx?.req;
  let pageProps = {
    city: DEFAULT_CITY,
  }
  let e
  if (component.getInitialProps) {
    try {
      pageProps = await component.getInitialProps(appContext)
    } catch (error) {
      e = error
    }
  }
  return { pageProps, e }
};


/* === HOT RELOAD FOR next-i18next === */
if (process.env.NODE_ENV !== 'production') {
  if (typeof window !== 'undefined') {
    const { applyClientHMR } = require('i18next-hmr/client');
    applyClientHMR(() => i18n);
  } else {
    const { applyServerHMR } = require('i18next-hmr/server');
    applyServerHMR(() => i18n);
  }
}
/* ===================================== */


export default appWithTranslation(App);
