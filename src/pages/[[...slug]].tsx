/** библиотеки */
import { NextPage, GetStaticProps, GetStaticPaths } from 'next';

/** компоненты */
import Error from './_error';
import { default as PageContent} from '../components/Page/Page';
import { useTranslation } from "next-i18next";
import { UsedLocales } from "front.ui.lib";

/** api */
import {
  getPage,
  getMenu,
  getPageBySlug,
} from '~/api/api';
import {getPosts, getSinglePost} from "~/api/blog";
/** интерфейсы */
import { PageProps } from '~/interfaces/Page.interface';
import { ProviderData } from '~/components/Providers/ProviderData.types';
import {DEFAULT_DESCRIPTION, DEFAULT_TITLE} from "~/constants/common";
import SeoProvider from "../components/Providers/Seo.provider";
import {serverSideTranslations} from "next-i18next/serverSideTranslations";
import Blog from "~/components/Page/Blog";


export const Page: NextPage<PageProps> = ({
  page,
  menu,
  preview,
  error,
  providerData
}: PageProps) => {
  const { i18n } = useTranslation('common');
  const lang = i18n.language;

  if (error && error.statusCode !== 404) {
    return <Error {...error} />;
  }

  const title = lang === UsedLocales.ru ? page.title : page.title_en ?? DEFAULT_TITLE;
  const description = lang === UsedLocales.ru ? page.meta_description : page.meta_description_en ?? DEFAULT_DESCRIPTION;

  return (
    <>
      <SeoProvider title={title} description={description}/>
      {providerData.data?.blog?.posts?.length && <Blog posts={providerData.data?.blog?.posts} page={page} menu={menu} preview={preview} />}
      <PageContent page={page} menu={menu} preview={preview} />
    </>
  );
};

export const getStaticPaths: GetStaticPaths = async () => {
  const page = await getPage();

  if (!page) return { paths: [], fallback: true };

  const paths = page.map(({ slug }) => ({
    params: { slug: slug.split('/') },
  }));

  return { paths, fallback: true };
};

export const getStaticProps: GetStaticProps = async ({
  params,
  preview = null,
  locale,
}) => {
  let page = {};

  const slug = params.slug === undefined ? 'index' : params.slug;
  const fullSlug = Array.isArray(slug)
    ? slug.map((s) => encodeURIComponent(s)).join('/')
    : encodeURIComponent(slug);

  const providerData: ProviderData = {
    data: await routeData(fullSlug)
  };

  let responseProps = {
    slug: fullSlug,
    providerData,
    preview,
    ...(await serverSideTranslations(locale ?? 'en', ['common'])),
  }

  try {
    page = await getPageBySlug(fullSlug);
    const menu = await getMenu(preview);
    return {
      revalidate: Number(process.env.REVALIDATE_STATIC_PERIOD),
      props: {
        ...responseProps,
        page: page,
        menu: menu,
        error: null,
      },
    };
  } catch (e) {
    return {
      revalidate: Number(process.env.REVALIDATE_STATIC_PERIOD),
      props: {
        ...responseProps,
        menu: {},
        error: e ?? null,
      },
    };
  }
};

const routeData = async (slug) => {
  const segments = slug.split('/');

  if (slug === 'blog') {
    const posts = await getPosts()
    return {
      blog: {
        posts
      }
    }
  }

  if (slug.match(/(blog\/\S*\b)/)) {
    const posts = await getSinglePost(segments[1])
    return {
      blog: {
        posts
      }
    }
  }
  return {};
};

export default Page;
