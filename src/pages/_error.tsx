import { ErrorProps } from "~/interfaces/Error.interface";

const Error = ({ statusCode, errorMessage }: ErrorProps): JSX.Element => {
  return (
    <div>
      <h1>Ошибка {statusCode}</h1>
      {errorMessage && <p>{errorMessage}</p>}
    </div>
  );
};

export default Error;
