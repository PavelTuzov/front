/* eslint-disable react/no-danger */
import Document, { Head, Main, NextScript, Html } from 'next/document';

export default class MyDocument extends Document {
  render() {
    return (
      <Html lang="ru">
        <Head>
          <noscript>

          </noscript>
        </Head>
        <body>
          {process.env.IS_PROD && (
            <noscript
              dangerouslySetInnerHTML={{
                __html: ``,
              }}
            />
          )}
          <Main />
          <div id="portal" />
          <NextScript />
        </body>
      </Html>
    );
  }
}
