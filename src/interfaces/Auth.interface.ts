/** Авторизационные данные */
export interface Auth {
  /** Идентификатор пользователя */
  id: number;
  /** Имя пользователя */
  name: string;
  /** Идентификатор города */
  cityId: number;
  /** Токен */
  token: string;
}
