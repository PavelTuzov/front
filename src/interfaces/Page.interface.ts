import { PageInfo, Menu } from '~/components/Page/types';
import { ProviderData } from '~/components/Providers/ProviderData.types';
import { ErrorProps } from './Error.interface';
import { Block } from "~/interfaces/Block.interface";

export interface PageProps {
  page: PageInfo;
  slug: string;
  providerData: ProviderData;
  menu: Menu;
  preview?: boolean;
  error?: ErrorProps;
  errorSlug: PageInfo;
  blocks: Block[];
}

export interface PageBlogProps extends PageProps {
  posts: any[];
}
