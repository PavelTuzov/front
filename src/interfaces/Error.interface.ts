export interface ErrorProps {
  statusCode?: number;
  errorMessage?: string;
}
