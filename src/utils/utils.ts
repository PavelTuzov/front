/**
 * Возвращает строку в формате 123 345 567,89
 * @param amountNumber номер
 * @param hideDecimal спрятать числа после запятой
 * @param separatorChar разделитель, использующийся вместо точки
 */
export const formatNumber = (
  amountNumber: number,
  hideDecimal?: boolean,
  separatorChar?: string,
): string => {
  const amount = amountNumber || 0;
  let res = amount
    .toFixed(hideDecimal ? 0 : 2)
    .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1\u00A0')
    .replace('.', `${separatorChar ?? ','}`);
  if (!hideDecimal && res.endsWith(',00')) {
    res = res.replace(',00', '');
  }
  return res;
};

/**
 * Склонение слова в зависимости от числа
 * @param {Number} number Число на основе которого нужно сформировать окончание
 * @param {Array} titles1 Массив слов (например ['символ', 'символа', 'символов'])
 * @param {Array} titles2 Массив слов (например ['Остался', 'Осталось', 'Осталось']) - необязательный параметр
 * @param {boolean} hideCount Флаг необходимости сокрытия числа при возврате результата (вернёт только склоняемое слово)
 * @return {String} пример "Осталось 12 символов"
 */
export const pluralizeAll = (
  number: number,
  titles1: string[],
  titles2?: string[],
  hideCount?: boolean,
): string => {
  const cases = [2, 0, 1, 1, 1, 2];

  const wordVariant: number =
    number % 100 > 4 && number % 100 < 20
      ? 2
      : cases[number % 10 < 5 ? number % 10 : 5];

  return `${titles2 ? `${titles2[wordVariant]} ` : ''}${
    !hideCount ? number : ''
  } ${titles1[wordVariant]}`;
};

/* yaspeller ignore:start */
/**
 * Возвращает первое вхождение директории по url
 * например 'http://localhost:3000/overtv/hitsuper20'
 * вернет   'overtv'
 * @param {string} url ссылка (например 'http://localhost:3000/overtv/hitsuper20')
 * @returns {string}
 */
export const parseDirectoryUrl = (url: string): string => {
  /** шаблон для поиска */
  const result = url.match(/(\w+:\/\/)(([A-z-.0-9А-я]+)(:\d+)?)\/(\w+)/);
  /** если найдено возвращает пятую группу поиска */
  return result ? result[5] : '';
};
/* yaspeller ignore:end */

/**
 * Проверяет, является ли ссылка внешней
 * @param {string} url ссылка
 * @returns {string}
 */
export const isExternal = (url: string): boolean => {
  if (!url || url.length === 0) return false;
  const match = url.match(
    /^([^:/?#]+:)?(?:\/\/([^/?#]*))?([^?#]+)?(\?[^#]*)?(#.*)?/,
  );
  if (
    typeof match[1] === 'string' &&
    match[1].length > 0 &&
    match[1].toLowerCase() !== window.location.protocol
  )
    return true;
  if (
    typeof match[2] === 'string' &&
    match[2].length > 0 &&
    match[2].replace(
      new RegExp(
        `:(${{ 'http:': 80, 'https:': 443 }[window.location.protocol]})?$`,
      ),
      '',
    ) !== window.location.host
  )
    return true;
  return false;
};

/**
 * Возвращает имя клиента из строки, содержащей ФИО
 * @param clientName
 */
export const getClientName = (clientName: string): string => {
  const clientNameSegments = clientName.split(' ');

  if (clientNameSegments.length >= 2) {
    return clientNameSegments[1];
  }

  return clientName;
};

/**
 * Формирует текст приветствия исходя из времени суток
 */
export const getGreetingText = (): string => {
  const GREETING_TEXT = {
    MORNING: 'Доброе утро',
    AFTERNOON: 'Добрый день',
    EVENING: 'Добро пожаловать домой',
    NIGHT: 'Доброй ночи',
  };
  const dateNow = new Date();
  const nowH = dateNow.getHours();
  const nowM = dateNow.getMinutes();
  const nowS = dateNow.getSeconds();
  let text = '';

  switch (true) {
    case nowH >= 0 && nowH < 6:
      text = GREETING_TEXT.NIGHT;
      break;
    case nowH >= 6 && nowH < 12:
      text = GREETING_TEXT.MORNING;
      break;
    case nowH >= 12 && nowH < 18:
      text = GREETING_TEXT.AFTERNOON;
      break;
    case nowH >= 18 && nowH < 23:
      text = GREETING_TEXT.EVENING;
      break;
    case nowH === 23 && nowM <= 59 && nowS <= 59:
      text = GREETING_TEXT.EVENING;
      break;
    default:
      text = '';
  }
  return text;
};

/**
 * Форматирует телефон для вывод на экран +7 (999) 999-99-99
 * @param phoneNumber
 * @returns string
 */
export const maskPhone = (phoneNumber: string) => {
  // получаем телефон формата 9990009999 (без +7 чисто цифры)
  let phone = phoneNumber.replace('+7', '').replace(/[^0-9]/g, '');

  if (phone.length && (phone[0] === '8' || phone[0] === '7'))
    phone = phone.slice(1, phone.length);

  // дробление телефона на части
  const phonePart = phone.match(/(\d{0,3})(\d{0,3})(\d{0,2})(\d{0,2})/) || '';

  return `+7 ${
    !phonePart[2]
      ? phonePart[1]
      : `(${phonePart[1]}) ${phonePart[2]}${
        phonePart[3] ? `-${phonePart[3]}` : ''
      }${phonePart[4] ? `-${phonePart[4]}` : ''}`
  }`;
};

/**
 * Сортирует массив объектов array по численному массиву order
 * Сложность O(n)
 * @param {Array} array Массив объектов
 * @param {Array} order Численный массив для сортировки
 * @param {String} key Имя свойства элемента array, по которому необходимо сортировать
 * @returns {Array} Отсортированный массив array
 */
export const mapOrderArray = (
  array: Array<any>,
  order: Array<any>,
  key: string,
): Array<any> => {
  const map = new Map();
  let index = 0;
  let tmp;
  if (!array || !order || array.length !== order.length) return array;
  array.forEach((it) => map.set(it[key], index++));
  if (order.some((it) => map.get(it) === undefined)) return array;
  index--;
  for (; index >= 0; index--) {
    if (array[index][key] !== order[index]) {
      tmp = array[index];
      array[index] = array[map.get(order[index])];
      array[map.get(order[index])] = tmp;
      map.set(tmp[key], map.get(order[index]));
    }
  }
  return array;
};
