import {
  IOutsideClickHelper,
  OutsideClickHelper,
} from 'front.ui.lib';

export const outsideClickHelper: IOutsideClickHelper = OutsideClickHelper();
