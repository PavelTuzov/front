import { Dispatch, SetStateAction } from 'react';

// generic для типизации хука
export type HooksTyping<hookType> = [
  hookType,
  Dispatch<SetStateAction<hookType>>,
];
