/** библиотеки */
import fetch from 'isomorphic-unfetch';
import Cookies, { CookieChangeOptions } from 'universal-cookie';

/** интерфейсы */
import { Auth } from '~/interfaces/Auth.interface';

/** константы */
import { TOKEN } from '~/constants/common';
import * as https from "https";
import {PageInfo, RecallFormResponse} from "~/components/Page/types";
import {DepartmentListResponse, ManagerListResponse, TagsListResponse} from "~/components/common/Manager/types";
import {TagProps} from "~/components/TagTech/interfaces";


const isClient = typeof window !== 'undefined';

const baseUrl: string = process.env.API_URL;

const parsedBaseUrl = new URL(baseUrl);

const options =
  !isClient && parsedBaseUrl.protocol === 'https:'
    ? {
        agent: new https.Agent({
          rejectUnauthorized: false,
        }),
      }
    : {};

const reqData: RequestInit = {
  //mode: process.env.NODE_ENV === 'production' ? 'cors' : 'no-cors',
  mode: process.env.NODE_ENV === 'production' ? 'cors' : 'cors',
  ...{
    ...options,
  },
};


export const cookies = new Cookies();
let token = cookies.get(TOKEN);

cookies.addChangeListener((params: CookieChangeOptions) => {
  if (params.name === TOKEN) {
    token = String(params.value);
  }
});

/**
 * Добавляет заголовок авторизации к объекту заголовков
 * @param headers {object} объект с заголовками
 * @return {object} объект со старыми заголовками и авторизацией
 */
export const getHeadersWithAuth = (headers?: HeadersInit | null): HeadersInit =>
  headers
    ? { ...headers, Authorization: `Bearer ${token}` }
    : { Authorization: `Bearer ${token}` };

/**
 * Проверка авторизации
 */
export const checkAuth = async (): Promise<{ status: number }> => {
  const data = await fetch(`${baseUrl}/Security/Auth/CheckAuth`, {
    ...reqData,
    headers: getHeadersWithAuth(),
  });
  return { status: data.status };
};

/** Авторизация по логину и паролю */
export const authByPassword = async (
  contractName: string,
  password: string,
): Promise<Auth> => {
  const requestData = {
    contractName,
    password,
  };
  const data = await fetch(
    `${baseUrl}/Security/Auth/AuthByPassword?contractName=${contractName}&password=${password}`,
    {
      ...reqData,
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
    },
  );

  return await data.json();
};


export const getUserInfo = async (): Promise<Auth> => {
  const data = await fetch(
    `${baseUrl}/Security/Auth/getData`,
    {
      ...reqData,
      method: 'GET',
      headers: { 'Content-Type': 'application/json' },
    },
  );

  return await data.json();
}


/** Возвращает данные для Меню.
 */
export const getMenu = async (preview: boolean): Promise<PageInfo> => {
  const data = await fetch(
    `${baseUrl}/menu/`,
    reqData,
  );

  return await data.json();
};

/**
 * Возвращает все Ленты.
 */
export const getPage = async (): Promise<PageInfo[]> => {
  const data = await fetch(`${baseUrl}/pages`, reqData);

  return await data.json();
};

export const getPageBySlug = async (slug): Promise<PageInfo> => {
    const data = await fetch(`${baseUrl}/pages/${slug}`, reqData);

    return await data.json();
};

export const makeFormOrder = async (formData): Promise<RecallFormResponse> => {
  return await fetch(`${baseUrl}/form`, {
    ...reqData,
    body: formData,
    method: 'POST',
  });
};

export const getManagers = async (): Promise<ManagerListResponse> => {
  const data = await fetch(`${baseUrl}/managers/online`, reqData);

  return await data.json();
};

export const getDepartments = async (): Promise<DepartmentListResponse> => {
  const data = await fetch(`${baseUrl}/managers/departments`, reqData);

  return await data.json();
};


export const getTechList = async (): Promise<TagsListResponse> => {
  const data = await fetch(`${baseUrl}/tech`, reqData);

  return await data.json();
};

export const getTech = async (id): Promise<TagProps> => {
  const data = await fetch(`${baseUrl}/tech/${id}`, reqData);

  return await data.json();
};
