/** Интерфейсы */
import {City} from "../interfaces/City.interface";
import {Auth} from "../interfaces/Auth.interface";
/** id cookies хранящей - "token" */
export const TOKEN = 'token';
/** id cookies хранящей - "Данные контракта" */
export const CONTRACT_AUTH = 'auth';
/** Время смерти cookies и конец авторизации */
export const COOKIES_DEAD_TIME_IN_MINUTES = 20 * 60;
/** Время смерти cookies при выборе города */
export const COOKIES_CITY_DEAD_TIME_IN_DAYS = 999;

/** Заголовок стандартный */
export const DEFAULT_TITLE = 'Заголовок сайта';
/** Описание стандартное */
export const DEFAULT_DESCRIPTION = 'Описание сайта';
/** Город, выбираемый по умолчанию */
export const DEFAULT_CITY: City = {
    id: 1,
    name: 'Екатеринбург',
};


/** Данные контракта по умолчанию */
export const DEFAULT_CONTRACT_AUTH: Auth = {
    id: 0,
    name: 'userName',
    cityId: 1,
    token: '',
};
