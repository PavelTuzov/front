module.exports = {
  debug: process.env.NODE_ENV === 'development',
  i18n: {
    defaultLocale: 'ru',
    locales: ['en', 'ru'],
    localeDetection: true,
  },
  trailingSlash: true,

};