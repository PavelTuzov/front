module.exports = {
    "extends": [
        "airbnb",
        "plugin:@typescript-eslint/recommended",
        "plugin:@typescript-eslint/eslint-recommended",
        "plugin:prettier/recommended",
        "plugin:import/errors",
        "plugin:import/warnings",
        "plugin:import/typescript",
        "next"
    ],
    "plugins": [
        "@typescript-eslint",
        "react-hooks"
    ],
    "parser": "@typescript-eslint/parser",
    "env": { "browser": true, "node": true, "es6": true },
    "parserOptions": {
        "sourceType": "module"
    },
    "rules": {
        "prettier/prettier": ["error", {
            "endOfLine": "auto"
        }],
        "react/prop-types": "off",
        "jsx-a11y/anchor-is-valid": "off",
        "import/prefer-default-export": "off",
        "import/no-unresolved": "off",
        "react/jsx-uses-react": "off",
        "react/react-in-jsx-scope": "off",
        "no-use-before-define": "off",
        "@typescript-eslint/no-use-before-define": "off",
        "no-shadow": "off",
        "@typescript-eslint/no-shadow": ["error"],
        "no-param-reassign": ["error", { "props": false }],
        "react/jsx-filename-extension": [
            "error",
            {
                "extensions": [
                    ".js",
                    ".jsx",
                    ".ts",
                    ".tsx"
                ]
            }
        ],
        "import/extensions": [
            "error",
            "ignorePackages",
            {
                "js": "never",
                "jsx": "never",
                "ts": "never",
                "tsx": "never",
                "d.ts": "never"
            }
        ],
        "quotes": ["error","single"],
        "react/require-default-props": "off",
        "no-plusplus": "off",
        "jsx-a11y/click-events-have-key-events": "off",
        "jsx-a11y/label-has-associated-control": "off",
        "import/order": "off",
        "no-case-declarations": "off",
        "jsx-a11y/no-static-element-interactions": "off",
        "import/no-extraneous-dependencies": "off",
        "react/jsx-props-no-spreading" : "off",
        "no-useless-escape" : "off",
        "import/no-cycle" : "off",
        "import/no-named-as-default": "off",
        "react/button-has-type" : "off",
        "react/no-unused-prop-types": "off",
        "@typescript-eslint/ban-ts-ignore" : "off",
    },
    settings: {
        'import/extensions': ['.js', '.jsx', '.ts', '.tsx', '.d.ts'],
        'import/parsers': {
            '@typescript-eslint/parser': ['.ts', '.tsx', '.d.ts'],
        },
        'import/resolver': {
            alias: {
                map: [['~', './src']],
                extensions: ['.js', '.jsx', '.ts', '.tsx', '.mjs'],
            },
        },
    },
};
