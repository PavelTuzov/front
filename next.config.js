/* eslint @typescript-eslint/no-var-requires: 0 */
const path = require('path');
const { i18n } = require('./next-i18next.config');


const settings = require('./settings.json');
const settingsDev = require('./settings.Development.json');

module.exports = () => {
  const isProd = !process.env.APP_ENV && process.env.NODE_ENV === 'production';
  const isDev =
    process.env.APP_ENV === 'development' ||
    process.env.NODE_ENV === 'development';

  console.log(`isDev:${isDev}  isProd:${isProd} `);

  const actualSettings = isProd
    ? settings
      : isDev
        ? settingsDev
        : null;

  const env = {
    API_URL: (() => {
      if (actualSettings)
        return process.env.API_URL ? process.env.API_URL : actualSettings.api;
      return 'API_URL:not (isDev,isProd && !isStaging,isProd && isStaging)';
    })(),

    REVALIDATE_STATIC_PERIOD: (() => {
      if (actualSettings)
        return process.env.REVALIDATE_STATIC_PERIOD
          ? process.env.REVALIDATE_STATIC_PERIOD
          : actualSettings.revalidateStaticPeriod;
    })(),

    COOKIE_DOMAIN: (() => {
      if (actualSettings)
        return process.env.COOKIE_DOMAIN ? process.env.COOKIE_DOMAIN : actualSettings.cookie.domain;
      return 'COOKIE_DOMAIN:not (isDev,isProd && !isStaging,isProd && isStaging)';
    })(),
    IS_DEV: isDev,
    IS_PROD: isProd
  };

  return {
    webpack: (config, options) => {
      const isServer = {...options};
      config.resolve.alias = {
        ...config.resolve.alias,
        '~': path.resolve(__dirname, './src'),
        react: path.resolve(__dirname, './node_modules/react'),
      };
      if (!isServer) config.resolve.fallback.fs = false;

      if (!options.isServer && config.mode === 'development') {
        // console.log('Watch path with locales: ');
        // console.log(path.resolve(__dirname, 'public/locales'));
        const { I18NextHMRPlugin } = require('i18next-hmr/plugin');
        config.plugins.push(
          new I18NextHMRPlugin({
            localesDir: path.resolve(__dirname, 'public/locales')
          })
        );
      }

      return config;
    },
    env,
    i18n,
    async extends() {
      return [
        'plugin:@next/next/recommended',
        "eslint:recommended",
        "next"
      ]
    }
  };
};
